import React, { Component } from "react";

import logo from "./logo.svg";
import Navegacion from "./components/navegacion/navegacion";
import Restaurante from "./components/restaurante/restaurante";
import Menu from "./components/menu/menu";
import Home from "./components/home/home";
import Carousel from "./components/carousel/carousel";
import Login from "./components/login/login";
import Registro from "./components/registro/registro";
import Nosotros from "./components/nosotros/nosotros";
import "font-awesome/css/font-awesome.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Foot from "./components/foot/foot";
import Contactenos from "./components/contactenos/contactenos";
import Terminos from "./components/terminos-condiciones/terminos-condiciones";
import Preguntas from "./components/preguntas-frecuentes/preguntas-frecuentes";
import PerfilRestaurante from "./components/perfil-restaurante/perfil-restaurante";
import PerfilRestaurante2 from "./components/perfil-restaurante/perfil-restaurante-2";
import OrdenPedido from "./components/orden-pedido/orden-pedido";
import Check from "./components/check-out/check-out";
import Tracker from "./components/check-out-tracker/check-out-tracker";
import Pedidos from "./components/pedidos/pedidos";
import Paso1 from "./components/check-out-paso-1/check-out-paso-1";
import Paso2 from "./components/check-out-paso-2/check-out-paso-2";
import Calificacion from "./components/calificacion/calificacion";
import Reclutamiento from "./components/reclutamiento/reclutamiento";
import PedidosHijos from "./components/pedidos/PedidosHijos/PedidosHijos";

//implementacion de redux

import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import reducer from "./store/reducers/";
import createHistory from "history/createBrowserHistory";

const store = createStore(reducer, {}, applyMiddleware(logger, thunk));
const history = createHistory();
class App extends Component {
  render() {
    return (
      <div className="App">
        <div>
          <div className="App-header">
            <Menu />
          </div>
          <Provider store={store}>
            <BrowserRouter history={history}>
              <Switch>
                <Route path="/restaurante" component={Restaurante} />
                <Route path="/home" component={Home} />
                {/* <Route path="/menu" component={Menu} /> */}
                <Route path="/carousel" component={Carousel} />
                <Route path="/login" component={Login} />
                <Route path="/registro" component={Registro} />
                <Route path="/contactenos" component={Contactenos} />
                <Route path="/nosotros" component={Nosotros} />
                <Route path="/terminos-condiciones" component={Terminos} />
                <Route path="/preguntas-frecuentes" component={Preguntas} />
                <Route
                  path="/perfil-restaurante"
                  component={PerfilRestaurante}
                />
                <Route
                  path="/perfil-restaurante-2"
                  component={PerfilRestaurante2}
                />
                <Route path="/orden-pedido" component={OrdenPedido} />
                <Route path="/check-out" component={Check} />
                <Route path="/check-out-tracker" component={Tracker} />
                <Route path="/pedidos" component={Pedidos} />
                <Route path="/check-out-paso-1" component={Paso1} />
                <Route path="/check-out-paso-2" component={Paso2} />
                <Route path="/calificacion" component={Calificacion} />
                <Route path="/reclutamiento" component={Reclutamiento} />
                <Route path="/pedido-hijos" component={PedidosHijos} />
                {/* <Route path="/LoginPage" component={LoginPage} /> */}
                {/*<Route path="/MapContainer" component={MapContainer} /> */}
              </Switch>
            </BrowserRouter>
          </Provider>
        </div>
      </div>
    );
  }
}

export default App;
