import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyA5FtjAiPqQeUnCHNCri6_FbCW3QKmpFRU",
    authDomain: "gst-realtime.firebaseapp.com",
    databaseURL: "https://gst-realtime.firebaseio.com",
    projectId: "gst-realtime",
    storageBucket: "gst-realtime.appspot.com",
    messagingSenderId: "66248684061"
  };

const fire  =firebase.initializeApp(config);

const database = firebase.database();
const firestore = firebase.firestore();
const message = firebase.messaging();

export {
  database,
  message,
  firestore,
  fire
};
