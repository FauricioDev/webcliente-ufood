import { GET_API_LOGIN, GET_API_REGISTER } from '../types';

const initialState = {
    data: ''
};

const data = (state = initialState, action) => {
    switch (action.type) {

        case GET_API_LOGIN:
            return { ...state, data: action.payload, };
        case GET_API_REGISTER:
            return { ...state, data: action.payload, };
        default:
            return state;
    }
}
export default data;