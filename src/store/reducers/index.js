import {combineReducers} from 'redux';
import login from './login';
import orden from './orden';

const rootReducer = combineReducers ({
    login,
    orden
});

export default rootReducer;