import { POST_API_CREATE_ORDEN } from '../types';

const initialState = {
    orden: {
        productos: [
        ],
        calificacion: 0,
        total: 0,
        descripcion: '',
        direcion: '',
        fecha_creacion: new Date(),
        order_type: null
    }
};

const orden = (state = initialState, action) => {
    switch (action.type) {

        case POST_API_CREATE_ORDEN:
            return { ...state, orden: action.payload, };
        default:
            return state;
    }
}
export default orden;