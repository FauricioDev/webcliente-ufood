import { GET_API_LOGIN } from '../types';
import axios from 'axios';

export const getApiLogin =(payload)=>{
    return {
        type: GET_API_LOGIN,
        payload
    };
};

export const getApiLoginAsync = ( email, contrasena)=> (dispatch) =>{
    return axios.post('http://appufood.com:5000/api/v1/auth/login', {
        email,
        contrasena,
        firetoken: 0,
        type : 'web'   
    })
    .then((res)=>{
        if(res != undefined){
            const loginCode = res['data'].code;
            const token = res['data'].token;            
            if(loginCode === 100){
                localStorage.setItem('userToken', token);  
            }                
           return res['data'];                        
        }
        return null  
    })
    .catch(e=>{
        console.error('error al tratar de loguearse', e)
        return e
    })        
}