import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';
import createHistory from 'history/createBrowserHistory';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { message } from './config/firebase';
import Paso1 from './components/check-out-paso-1/check-out-paso-1';
import Pedidos from './components/pedidos/pedidos';
import PedidoHijos from './components/pedidos/PedidosHijos/PedidosHijos';
import Calificacion from "./components/calificacion/calificacion";

import { store } from './_helpers';

// ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter history={createHistory}>
        <Switch>
          <Route path="/" exact component={App}/>
          <Route path="/home" component={App} />
          <Route path="/restaurante" component={App} />
          <Route path="/menu" component={App} />
          <Route path="/carousel" component={App}/>
          <Route path="/login" component={App}/>
          <Route path="/registro" component={App}/>
          <Route path="/contactenos" component={App} />
          <Route path="/nosotros" component={App} />
          <Route path="/terminos-condiciones" component={App} />
          <Route path="/preguntas-frecuentes" component={App} />
          <Route path="/perfil-restaurante" component={App} />
          <Route path="/perfil-restaurante-2" component={App} />
          <Route path="/orden-pedido" component={App} />
          <Route path="/check-out" component={App} />
          <Route path="/check-out-tracker" component={App} />
          <Route path="/pedidos" component={Pedidos} />
          <Route path="/check-out-paso-1" component={Paso1} />
          <Route path="/check-out-paso-2" component={App} />
          <Route path="/calificacion" component={Calificacion} />
          <Route path="/reclutamiento" component={App} />
          <Route path="/PedidoHijos" component ={PedidoHijos} />

          {/* <Route path="/LoginPage" component={App}/> */}


        {/* <Route paht="/MapContainer" component={App} />> */}
        </Switch>
      </BrowserRouter>
    </Provider>,
    document.getElementById('root')
  )
registerServiceWorker();

// logging our messaging notification
message.onMessage(payload => {
  console.log(payload);
})
