import React, { Component } from 'react';
import  {Carousel, CarouselCaption, CarouselInner, CarouselItem, View, Mask, Container, Row, Col, CardGroup, Card, CardText, CardBody, CardImage, CardTitle, Button, Footer, Table, Input, Fa } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import './foot.css'
import logo from '../../img/logo-blanco.png'


class Foot extends Component {
    
    render() {
        return(
            <div>
                <Footer style={{backgroundColor:'#222d35', color:'#767c80'}} className="font-small">
                    <Container className="pt-5">
                        <Row className="justify-content-between">
                            <Col md="3" className="ml-5">
                                <img src={logo} height="120" className="d-inline-block align-top mr-5"/>
                                <Table small className='table table-unruled'>
                                    {/* <thead>
                                    
                                    </thead> */}
                                    <tbody>
                                        <tr>
                                        <td>Monday - Thursday</td>
                                        <td>11:00 - 21:00</td>
                                        </tr>
                                        <tr>
                                        <td>Friday - Saturday</td>
                                        <td>11:30 - 22:00</td>
                                        </tr>
                                        <tr>
                                        <td>Sundays</td>
                                        <td>12:00 - 20:00</td>
                                        </tr>
                                        <tr>
                                        <td>
                                            <br/>
                                            <p>Follow us:</p>
                                        </td>
                                        <td>
                                            <br/>
                                            <p><i className="fa fa-facebook-official fa-2x pr-3" aria-hidden="true"></i>
                                            <i className="fa fa-twitter fa-2x pr-3" aria-hidden="true"></i>
                                            <i className="fa fa-instagram fa-2x" aria-hidden="true"></i></p>
                                        </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Col>
                            <Col md="4" className="pt-5 mt-5">
                                <ul>
                                <li className="list-unstyled"><i className="fa fa-map-marker fa-2x"></i> Cali Colombia</li>
                                <br/>
                                <li className="list-unstyled"><i className="fa fa-mobile fa-2x"></i> +1 555 125 9455, +42 548 78 983</li>
                                <br/>
                                <li className="list-unstyled"><i className="fa fa-envelope fa-1x"></i> hello@ufood.com</li>
                                </ul>
                            </Col>
                        </Row>
                    </Container>
                    <div className="bg-footer-copyright py-4">
                        <br/>
                    </div>
                </Footer>
            </div>
        )
    }
}

export default Foot;
