import React, { Component } from "react";
import {
  Stepper,
  Step,
  Container,
  Row,
  Col,
  Button,
  Footer,
  Table,
  Input,
  Fa,
  View
} from "mdbreact";

import { Link } from "react-router-dom";
import "./pedidos.css";
import chulo from "../../img/icons/check1.png";
import Foot from "../foot/foot";
import Imagenes from "../imagenes/imagenes";

export const Lista = props => {
    console.log("======================");
    console.log("props: ",props);
    console.log("id hijo: ", props.idHijo);
    console.log("id padre:", props.PedidoPadre);
    localStorage.setItem('idCarritoHijo', props.idHijo);
    localStorage.setItem('carritoPadre', JSON.stringify(props.PedidoPadre))
    console.log("======================");
  return(

  <View className="container pedido1">
    <Row className="d-flex justify-content-between align-items-center z-depth-1 p-3 px-5">
      <Col-4>
        <p className="h1-responsive font-weight-bold" style={{ color: "red" }}>
          {props.index}
        </p>
      </Col-4>
      <Col-4>
        {/* <p className="h4-responsive">Pedido # 5647</p> */}
        <h3 className="h3-responsive font-weight-bold" > {props.comercio}</h3>
        <p className="h6-responsive">Total Pedido: ${props.total}</p>
        <p>Tipo de Pago: Efectivo</p>
        <p>Fecha del pedido: {props.ultima_modificacion}</p>
      </Col-4>
      <Col-4>
        <p>Orden Recibida</p>
        {/* <Link to="/pedidoHijos"> */}
        <Link to={{
                pathname:props.ruta,
                params:{
                   PedidoPadre:props.idPedidoPadre, 
                   idHijo: props.idHijo
                  }
                }
              }
                >
            <Button 
                onClick={props.seeOrder}
                color="danger" className="px-3 badge-pill">
            Ver Pedido <Fa icon="chevron-right fa-2x" className="mx-1" />
            </Button>
        </Link>
      </Col-4>
    </Row>
  </View>
)};

export default Lista;
