import React, { Component } from "react";
import "../pedidos.css";
import Foot from "../../foot/foot";
import Lista from "../list-pedidos";
import url from '../../../config/url';
import Menu from '../../../components/menu/menu';
import {fire} from '../../../config/firebase';

const refOrders = fire.database().ref("orders");


class PedidosHijos extends Component {
    constructor(props){
        super(props);
        this.state={
            pedidos: {
              procesos: ""
            },
            token: null
        }
    }
    componentDidMount = async()=>{
        const token =  await localStorage.getItem('userToken');
        const idUser = await localStorage.getItem('idUser');
        const idOrderFather = await localStorage.getItem('idOrderFather');
        const { PedidoPadre } = this.props.location.params;
       
        console.log('el id del pedido padre',PedidoPadre);
        this.setState({token})
        this.setState({pedidos: PedidoPadre})
        // fetch(`${url}gst-ordenes/listar-ordenes-hijas-usuario/${idUser}/cliente`, { esto es para traer menudiatio los ordenes, es decir separado por comercios
        // fetch(`${url}gst-ordenes/listar-orden/${idPedidoPadre}`, {
        //     headers: {
        //         'Content-Type': 'application/json',
        //         Authorization: token
        //     }
        // })
        // .then(res=>res.json())
        // .then(pedidos =>{
        //     console.log('respuesta a traer todos los pedidos hijos', pedidos);
        //     this.setState({pedidos: pedidos.procesos});
        // })
        // .catch(err=>console.error('error al traer todos los pedidos', err));
        // refOrders.on("value", this.stateFirebase, err => console.log(err));

    }

    // componentWillUnmount = () => refOrders.off("value", this.stateFirebase, err => console.log(err));


    // stateFirebase = snap => {
    //   let obj = [];
    //   const temp = snap.val();
    //   for (let child in snap.val()) {
    //     const i = {
    //       ...temp[child],
    //       _id: child
    //     };
    //     obj = [...obj, i];
    //   }
    //   const { pedidos } = this.state;
      
    //   return this.setState({ pedidos: obj }, ()=> console.log('todos los pedidos hijos del tipo', this.state.pedidos));
    // };

    seeOrder =(idOrder)=>{
        localStorage.setItem('idOrder',idOrder);
        console.log(idOrder);
    }
    
  render() {
    return (
      <div>
      <div className="App-header" style={{marginBottom: 200}}>
                    <Menu />
        <div className="container pt-5">
          <ul
            className="breadcrumb bg-pagnav px-5"
            style={{ backgroundColor: "white" }}
          >
            <li className="breadcrumb-item">
              <a href="/home" style={{ color: "black" }}>
                Home
              </a>
            </li>
            <li className="breadcrumb-item">
              <a href="/#" style={{ color: "black" }}>
                Shop
              </a>
            </li>
            <li className="breadcrumb-item active">Food Tracker</li>
          </ul>
        </div>
        </div>
        <hr />
        <div className="container text-center py-5">
          <p className="h3-responsive font-weight-bold">Pedido descompuesto en carritos hijos</p>
          {  Array.isArray(this.state.pedidos.procesos) &&
          this.state.pedidos.procesos.map((item, index)=>{
            let total = 0;
            // item.productos.map((p,i)=>{
            //     total = Number(total) + Number(p.total);
            // })
              return(
                  <Lista
                    key={index}
                    index={index +1}
                    total={item.total}
                    ultima_modificacion={this.state.pedidos.ultima_modificacion}
                    seeOrder={()=>this.seeOrder(item._id)}
                    comercio={item.comercio}
                    ruta={"/check-out-tracker"}
                    idHijo={item.id}
                    PedidoPadre={this.props.location.params.PedidoPadre}
                  />
              )
          }).reverse()
          }
        </div>
        {/* <Imagenes /> */}
        <Foot />
      </div>
    );
  }
}

export default PedidosHijos;
