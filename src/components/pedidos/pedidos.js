import React, { Component } from "react";
import {
  Stepper,
  Step,
  Container,
  Row,
  Col,
  Button,
  Footer,
  Table,
  Input,
  Fa,
  View
} from "mdbreact";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import moment from 'moment';
import "./pedidos.css";
// import chulo from '../../img/icons/check.png'
// import chulo from '../../img/icons/chulo.png'
import chulo from "../../img/icons/check1.png";
import Foot from "../foot/foot";
import Imagenes from "../imagenes/imagenes";
import Lista from "./list-pedidos";
import url from '../../config/url';
import Menu from '../../components/menu/menu';
import {fire} from '../../config/firebase';

const refOrders = fire.database().ref("orders");


class Pedidos extends Component {
    constructor(props){
        super(props);
        this.state={
            pedidos: null,
            token: null
        }
    }
    componentDidMount = async()=>{
        const token =  await localStorage.getItem('userToken');
        const idUser = await localStorage.getItem('idUser');
        this.setState({token})
        // fetch(`${url}gst-ordenes/listar-ordenes-hijas-usuario/${idUser}/cliente`, { esto es para traer menudiatio los ordenes, es decir separado por comercios
        // fetch(`${url}gst-ordenes/listar-ordenes-usuario/cliente`, {
        //     headers: {
        //         'Content-Type': 'application/json',
        //         Authorization: token
        //     }
        // })
        // .then(res=>res.json())
        // .then(pedidos =>{
        //     console.log('respuesta a traer todos los pedidos', pedidos);
        //     this.setState({pedidos});
        // })
        // .catch(err=>console.error('error al traer todos los pedidos', err));
        refOrders.on("value", this.stateFirebase, err => console.log(err));
    }
      // componentWillUnmount = () => refOrders.off("value", this.stateFirebase, err => console.log(err));

      stateFirebase = snap => {
        let obj = [];
        const temp = snap.val();
        for (let child in snap.val()) {
          const i = {
            ...temp[child],
            _id: child
          };
          obj = [...obj, i];
        }
        const { pedidos } = this.state;
        
        return this.setState({ pedidos: obj }, ()=> console.log('todos los pedidos del tipo', this.state.pedidos));
      };

    seeOrder =(idOrderFather)=>{
        localStorage.setItem('idOrderFather',idOrderFather);
        console.log(idOrderFather);
    }
  render() {
    return (
      <div>
      <div className="App-header" style={{marginBottom: 200}}>
                    <Menu />
        <div className="container pt-5">
          <ul
            className="breadcrumb bg-pagnav px-5"
            style={{ backgroundColor: "white" }}
          >
            <li className="breadcrumb-item">
              <a href="/home" style={{ color: "black" }}>
                Home
              </a>
            </li>
            <li className="breadcrumb-item">
              <a href="/#" style={{ color: "black" }}>
                Shop
              </a>
            </li>
            <li className="breadcrumb-item active">Food Tracker</li>
          </ul>
        </div>
        </div>

        <hr />
        <div className="container text-center py-5">
          <p className="h3-responsive font-weight-bold">Tus Pedidos</p>
          { (Array.isArray(this.state.pedidos)) &&
          this.state.pedidos.map((item, index)=>{
            item.ultima_modificacion = moment(item.ultima_modificacion)
            .format()
            .split("-")
            .slice(0, 3)
            .join("-")
            .replace(/T/g, " - ")
            .split(":")
            .splice(0, 2)
            .join(":");
            // let total = 0;
            // item.procesos.map((p,i)=>{
            //   p.productos.map((j)=>{
            //     total = Number(total) + Number(j.total);
            //   });

            // })

            
              return(
                  <Lista
                    key={index}
                    index={index +1}
                    total={item.total}
                    ultima_modificacion={item.ultima_modificacion}
                    seeOrder={()=>this.seeOrder(item._id)}
                    idPedidoPadre={item}
                    idHijo={null}
                    ruta={"/pedidoHijos"}
                  />
              )
          }).reverse()
          }
        </div>
        {/* <Imagenes /> */}
        <Foot />
      </div>
    );
  }
}

export default Pedidos;
