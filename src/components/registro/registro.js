import React, { Component } from 'react';
import  { Input, Fa } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import './registro.css'
import logo from '../../img/logo-blanco.png'
import Foot from '../foot/foot';



class Registro extends Component {
    constructor(props){
        super(props);
        this.state = {
            nombre: '',
            password: '',
            telefono: '',
            email: '',
        }
        
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }
    
    onChange(e) {
        this.setState ({ [e.target.name]: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();
        console.log(this.state);
    }

    render() {
        return(
            
        <div>
            <div className="container py-5" style={{marginTop:150}}>
                <div className="row justify-content-center">
                    <div className="col-offset-3  col-md-6 col-sm-12 col-md-offset-3">
                        <form onSubmit={this.onSubmit}>
                            <p className="h1-responsive">Registrate</p>
                            <p style={{color:'#ff3e44'}}>___________</p>
                            <p style={{color:''}} className="">Crea una cuenta nueva con UFood</p>
                            <p style={{color:''}} className="text-center"><font size="2">Al registrar una cuenta aceptas los terminos y condiciones</font></p>
                            <input type="text" id="nombre" name="nombre" className="form-control py-3" style={{borderRadius:'50px'}} placeholder="Nombre Completo" value={this.state.nombre} onChange={this.onChange} />
                            <br/>
                            <input type="text" id="telefono" name="telefono" className="form-control py-3" style={{borderRadius:'50px'}} placeholder="Telefono" value={this.state.telefono} onChange={this.onChange} />
                            <br/>
                            <input type="email" id="email" name="email" className="form-control py-3" style={{borderRadius:'50px'}} placeholder="Email" value={this.state.email} onChange={this.onChange} />
                            <br/>
                            <input type="password" id="password" name="password" className="form-control py-3" style={{borderRadius:'50px'}} placeholder="Contraseña" value={this.state.password} onChange={this.onChange} />
                            <br/>
                            <div className="row d-flex align-items-center">
                                <div className="col-6 d-flex align-items-start">
                                    <select className="browser-default style-select form-control badge-pill" value={this.state.value} onChange={this.onChange} style={{borderRadius:'50px'}}>
                                        <option value="" disabled selected>Ciudad</option>
                                        <option value="cali">Cali</option>
                                        <option value="medellin">Medellin</option>
                                        <option value="bogota">Bogota</option>
                                        <option value="cartagena">Cartagena</option>
                                    </select>                                
                                </div>
                                <div className="col-6 d-flex justify-content-end">
                                    <select className="browser-default style-select form-control badge-pill" value={this.state.value} onChange={this.onChange}>
                                        <option value="" disabled selected>Universidad</option>
                                        <option value="autonoma">Autonoma</option>
                                        <option value="icesi">Icesi</option>
                                        <option value="univalle">Univalle</option>
                                        <option value="javeriana">Javeriana</option>
                                    </select>
                                </div>
                            </div>
                            {/* <Input label="Remember Me" filled type="checkbox" id="checkbox6" /> */}
                            {/* <input id="rememberme" name="rememberme" label="rememberme" filled type="checkbox" id="checkbox6" />
                            <label for="rememberme">Remember Me</label> */}
                            <br/>
                            <button className="btn btn-danger badge-pill btn-lg">Register</button>
                                
                        </form>
                    </div>
                </div>
            </div>
                <Foot/>

        </div>

        )
    }
}

export default Registro;
