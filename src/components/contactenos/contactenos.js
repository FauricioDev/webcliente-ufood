import React, { Component } from 'react';
import  { Input, Fa, View } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import './contactenos.css'
import logo from '../../img/logo-blanco.png'
import Foot from '../foot/foot';
import Imagenes from '../imagenes/imagenes';
import gris from '../../img/gris.png'



class Contactenos extends Component {
    constructor(props){
        super(props);
        this.state = {
            fname: '',
            lname:'',
            subject: '',
            message: ''
        }
        
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }
    
    onChange(e) {
        this.setState ({ [e.target.name]: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();
        console.log(this.state);
    }

    render() {
        return(
            
        <div>
            <div className="container py-5" style={{marginTop:150}}>
                            <p className="h1-responsive text-center">Contactanos</p>
                            <p className="grey-text text-center">UFood</p>
                <div className="row justify-content-center">
                    <div className="col-md-8 col-sm-12">
                        <form onSubmit={this.onSubmit}>
                            <p className="h4-responsive">Dejanos un mensaje</p>
                            <p style={{color:'#ff3e44'}}>___________</p>
                            <p className="grey-text">texto texto texto texto texto</p>
                            <div className="row">
                                <div className="col-6">
                                    <label for="fname"><strong>First Name</strong></label>
                                    <input type="text" id="fname" name="fname" className="form-control py-3" style={{borderRadius:'50px'}} placeholder="First Name" value={this.state.fname} onChange={this.onChange} />
                                </div>
                                <div className="col-6">
                                    <label for="lname"><strong>Last Name</strong></label>
                                    <input type="text" id="lname" name="lname" className="form-control py-3" style={{borderRadius:'50px'}} placeholder="Last Name" value={this.state.lname} onChange={this.onChange} />
                                </div>
                            </div>
                            <br/>
                            <label for="subject"><strong>Subject</strong></label>
                            <input type="text" id="subject" name="subject" className="form-control py-3" style={{borderRadius:'50px'}} placeholder="Subject" value={this.state.subject} onChange={this.onChange} />
                            <br/>
                            <label for="message"><strong>Message</strong></label>
                            <input type="textarea" id="message" name="message" className="form-control py-3" style={{borderRadius:'30px', height:'100px'}} placeholder="" value={this.state.message} onChange={this.onChange} />
                            <br/>
                            <button className="btn btn-danger badge-pill btn-lg">Send</button>
                                
                        </form>
                    </div>
                    <div className="col-md-4 col-sm-12 pl-3">
                            <p className="h4-responsive">Datos de contacto</p>
                            <p style={{color:'#ff3e44'}}>___________</p>
                            <p className="grey-text">texto texto texto texto texto</p>
                            <p><span className="grey-text">E-mail:</span> info@ufood.com</p>
                            <p><strong>Horario de Atencion</strong></p>
                            <div className="row">
                                <br/>
                                <div className="col-8 grey-text align-self-start">
                                   <p>Monday:</p>
                                   <p>Tuesday:</p>
                                   <p>Wednesday:</p>
                                   <p>Thursday:</p>
                                   <p>Friday:</p>
                                   <p>Saturday:</p>
                                   <p>Sunday:</p>
                                </div>
                                <div className="col-4 grey-text align-self-end">
                                   <p>12-6 PM</p>
                                   <p>12-6 PM</p>
                                   <p>12-6 PM</p>
                                   <p>12-6 PM</p>
                                   <p>12-6 PM</p>
                                   <p>12-4 PM</p>
                                   <p>Closed</p>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <p><strong>Trabaja con Nosotros</strong></p>
                            <p className="grey-text">If you're interested in employment opportunities at Pizzaro, please email us: <strong>admin@ufood.com</strong></p>

                                
                    </div>
                </div>
            </div>
            <Imagenes/>
            <Foot/> 
        </div>

        )
    }
}

export default Contactenos;
