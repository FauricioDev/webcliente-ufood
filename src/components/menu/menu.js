import React, { Component } from "react";
import {
  Navbar,
  NavbarBrand,
  NavbarNav,
  NavbarToggler,
  Collapse,
  NavItem,
  NavLink,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "mdbreact";
import { BrowserRouter as Router } from "react-router-dom";
// import './menu.css'
import { BrowserRouter, Redirect, Route, Switch, Link } from "react-router-dom";

import Pizza from "../../img/icons/pizza.png";
import Burgers from "../../img/icons/pizza.png";
import Salads from "../../img/icons/pizza.png";
import Tacos from "../../img/icons/pizza.png";
import Wraps from "../../img/icons/pizza.png";
import Fries from "../../img/icons/pizza.png";
import Drinks from "../../img/icons/pizza.png";
import Navegacion from "../navegacion/navegacion";
import Foot from "../foot/foot";
import $ from "jquery";
// import './carouselcards.js'

class Menu extends React.Component {
  // componentDidMount(){
  //     $("#myCarousel").on("slide.bs.carousel", function(e) {
  //         console.log('click')
  //         var $e = $(e.relatedTarget);
  //         var idx = $e.index();
  //         var itemsPerSlide = 3;
  //         var totalItems = $(".carousel-item-home").length;

  //         if (idx >= totalItems - (itemsPerSlide - 1)) {
  //           var it = itemsPerSlide - (totalItems - idx);
  //           for (var i = 0; i < it; i++) {
  //             // append slides to end
  //             if (e.direction == "left") {
  //               $(".carousel-item-home")
  //                 .eq(i)
  //                 .appendTo(".carousel-inner");
  //             } else {
  //               $(".carousel-item-home")
  //                 .eq(0)
  //                 .appendTo($(this).find(".carousel-inner"));
  //             }
  //           }
  //         }
  //       });
  // }

  render() {
    return (
      <div>
        <Navegacion />
        <div className="container-menu">
          <ul id="nav" className="d-flex justify-content-center">
            {/* Inicio menu Restaurantes */}
            {/* <li><a href="#s1">Restaurantes</a> */}
            <li className="">
              <a
                onClick={() => {
                  localStorage.setItem("comerceType", "restaurante");
                                }
                        }
                href="/restaurante"
              >
                Restaurantes
              </a>
              <span id="s1" />
              <ul id="nav2" className="subs">
                <Link to="/restaurante">
                    {/* 
                  <li className="text-center">
                    <a href="#ss1">Icesi</a>
                    <span id="ss1" />
                    <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul>
                  </li>
                                     */}
                </Link>
                  {/*
                <li className="text-center">
                  <a href="#ss1">Javeriana</a>
                  <span id="ss1" />
                  <span id="ss1" />
                   <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul>
                </li>
                                     */}
                {/* <li className="text-center"><a href="#ss1">Autonoma</a>
                                <span id="ss1"></span>
                                <span id="ss1"></span>
                                    <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul>
                            </li>
                            <li className="text-center"><a href="#ss1">Univalle</a>
                                <span id="ss1"></span>
                                <span id="ss1"></span>
                                    <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul>
                            </li> */}
              </ul>
            </li>
            {/* Fin menu Restaurantes */}
            {/* Inicio menu Emprendedores */}
            <li>
              <a
                onClick={() => {
                    localStorage.setItem("comerceType", "emprendimiento");
                }}
                href="/restaurante"
              >
                Emprendedores
              </a>
              <span id="s2" />
              {/* <ul id="nav2" className="subs">
                            <li className="text-center"><a href="#ss1">Icesi</a>
                                <span id="ss1"></span>
                                    <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul>
                            </li>
                            <li className="text-center"><a href="#ss1">Javeriana</a>
                                <span id="ss1"></span>
                                <span id="ss1"></span>
                                    <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul>
                            </li> */}
              {/* <li className="text-center"><a href="#ss1">Autonoma</a>
                                <span id="ss1"></span>
                                <span id="ss1"></span>
                                    <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul>
                            </li>
                            <li className="text-center"><a href="#ss1">Univalle</a>
                                <span id="ss1"></span>
                                <span id="ss1"></span>
                                    <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul>
                            </li> */}
              {/* </ul> */}
            </li>
            {/* Fin menu Emprendedores */}
            {/* Inicio menu Cafeterias */}
            <li>
              <a
                href="/restaurante"
                onClick={() => {
                    localStorage.setItem("comerceType", "cafeteria");
                }}
              >
                Cafeterias
              </a>
              {/* <span id="s3" />
              <ul id="nav2" className="subs">
                <li className="text-center">
                  <a href="#ss1">Icesi</a> */}
                  {/* <span id="ss1"></span> */}
                  {/* <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul> */}
                {/* </li>
                <li className="text-center">
                  <a href="#ss1">Javeriana</a> */}
                  {/* <span id="ss1"></span>
                                <span id="ss1"></span> */}
                  {/* <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul> */}
                {/* </li> */}
                {/* <li className="text-center"><a href="#ss1">Autonoma</a>
                                <span id="ss1"></span>
                                <span id="ss1"></span>
                                    <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul>
                            </li>
                            <li className="text-center"><a href="#ss1">Univalle</a>
                                <span id="ss1"></span>
                                <span id="ss1"></span>
                                    <ul id="nav3" className="subs2">
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Pizza </a>
                                        </li>
                                        <span className="vl2"></span>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Burgers </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Salads </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Tacos </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Wraps </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Fries </a>
                                        </li>
                                        <li><a href="#" className="sm">
                                            <img src={Pizza} height="20" /><br/>
                                        Drinks </a>
                                        </li>
                                    </ul>
                            </li> 
              </ul>
                            */}
            </li>
            {/* Fin menu Cafeterias */}
          </ul>
        </div>
      </div>
    );
  }
}

export default Menu;
