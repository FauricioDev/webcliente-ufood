import React, { Component } from 'react';
import  { Input, Fa } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import { Redirect } from 'react-router'

import './login.css'
import logo from '../../img/logo-blanco.png'
import Foot from '../foot/foot';

//redux
import { connect } from "react-redux";
import { getApiLoginAsync, getApiLogin } from '../../store/actions/login';

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            usuario: 'allensmilko@gmail.com',
            password: 'mP9231992'
        }
        
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);   

        // this.eventEmitter = new EventEmitter();     

    }
    
    onChange(e) {
        this.setState ({ [e.target.name]: e.target.value });
    }

    getDataUser =(dataUser)=>{

        const body1 ={
            order_type: null,
            productos: [],
            descripcion: '',
            direcion: '',
            fecha_creacion: new Date(),
            total: 0,
                  }
                  
        fetch(`http://appufood.com:5000/api/v1/auth/me`,{
            method:'GET',
            headers: {
                'Content-Type':'application/json',
                Authorization: dataUser.token
            }
        })
        .then((res)=>res.json())
        .then((res)=>{

            console.log('respuesta al traer toda la data del usuario', res);
            localStorage.setItem('idUser',res._id);

            if(dataUser.code != null){
                switch(dataUser.code){
                    case 100:
                        alert('Bienvenido')                                        
                        this.props.history.push('/')                    
                        window.location.reload()
                        localStorage.setItem('ordenes', JSON.stringify(body1));
                    break;
                    case 404:
                        alert('uppss Pailas')
                    break;
                }
            }

        })
        .catch(err=>console.error('error al traer la data del usuario', err))
    }
    onSubmit = async (e) =>{
       
        const { usuario, password} = this.state; 
        console.log('soy la data ',this.props.dataUser);
        e.preventDefault();
        console.log(this.state);
        const dataUser = await this.props.getApiLoginAsync(usuario, password); 
        this.getDataUser(dataUser);
        
       // this.props.getApiLogin(dataUser);
        //localStorage.setItem('userSession', true);
        console.log(dataUser)
       
    }

    render() {
        return(
            
        <div>
            <div className="container py-5" style={{marginTop:150}}>
                <div className="row justify-content-center">
                    <div className="col-offset-3  col-md-6 col-sm-12 col-md-offset-3">
                        <form onSubmit={this.onSubmit}>
                            <p className="h1-responsive">Inicio de Sesion</p>
                            <p style={{color:'#ff3e44'}}>___________</p>
                            <p style={{color:''}} className="py-3">Bienvenido de vuelta</p>
                            <label for="usuario"><strong>Usuario</strong></label>
                            <input type="text" id="usuario" name="usuario" className="form-control py-3" style={{borderRadius:'50px'}} placeholder="Email Address*" value={this.state.usuario} onChange={this.onChange} />
                            <br/>
                            <label for="password"><strong>Contraseña</strong></label>
                            <input type="password" id="password" name="password" className="form-control py-3" style={{borderRadius:'50px'}} placeholder="Pasword*" value={this.state.password} onChange={this.onChange} />
                            <div className="row d-flex align-items-center mb-4">
                                <div className="col-5 d-flex align-items-start">
                                    <Input label="Remember Me" filled type="checkbox" id="checkbox6" />
                                </div>
                                <div className="col-7 d-flex justify-content-end">
                                    <p className="grey-text">Forgotten Password?</p>
                                </div>
                            </div>
                            {/* <input id="rememberme" name="rememberme" label="rememberme" filled type="checkbox" id="checkbox6" />
                            <label for="rememberme">Remember Me</label> */}
                            <br/>
                            <button className="btn btn-danger badge-pill btn-lg">Login</button>
                                
                        </form>
                    </div>
                </div>
            </div>
                <Foot/>
        </div>

        )
    }
}

const mapStateToProps = (state, props) => {
        console.log('soy el state',state);
        console.log('soy los props', props)
    return{
        dataUser: state.login.data
    }
  };
  
  const mapDispatchToProps = {
    getApiLoginAsync,
    getApiLogin
  };

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login);