import React, { Component } from "react";
import {
  Stepper,
  Step,
  Container,
  Row,
  Col,
  Button,
  View,
  Mask,
  Input
} from "mdbreact";
import "./check-out-paso-2.css";
import url from "../../config/url";

class Contenido extends Component {
  constructor(props) {
    super(props);
    this.state = {
      radio2: false,
      ordenCompleta: null,
      precioTotal: 0,
      token: null,
      ubicacion: null,
      precio_domicilio: null
    };

    this.onClick4 = this.onClick4.bind(this);
    this.onClick5 = this.onClick5.bind(this);
  }

  componentDidMount = async () => {
   
    console.log("componentDidMount de confirmar la orden");
    const token = await localStorage.getItem("userToken");
    this.setState({ token });
    let ordenCompleta = await localStorage.getItem("ordenes");
    ordenCompleta = JSON.parse(ordenCompleta);
    this.setState({ ordenCompleta, precioTotal: ordenCompleta.total }, () =>
      console.log(
        "precio total de la orden completa",
        this.state.ordenCompleta.total
      )
    );
    ordenCompleta.procesos = ordenCompleta.productos;
    console.log("PAso 2 Soy la orden completa", ordenCompleta);
    delete ordenCompleta.productos
     console.log("pedido juan david: " ,this.calcularDomicilio(ordenCompleta));
  };

  onClick4() {
    this.setState({ radio2: 4 });
  }
  onClick5() {
    this.setState({ radio2: 5 });
  }

   

  confirmOrder = () => {
    const { token, ordenCompleta, ubicacion } = this.state;
    this.props.nextStep();
    ordenCompleta["descripcion"] = ubicacion;
    ordenCompleta["domicilio"] = 0;
    ordenCompleta["type"] = ordenCompleta["order_type"];
    console.log('lo que recibe ', ordenCompleta);

    console.log('orden completa', ordenCompleta);
    fetch(`${url}gst-ordenes/nueva-orden`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: token
      },
      body: JSON.stringify(ordenCompleta)
    })
      .then(res => res.json())
      .then(orden => {
        console.log("respuesta al crear la orden", orden);

        ordenCompleta.procesos.map((item, index) => {
          const ordenFinal = {...item,order_type: ordenCompleta.order_type, descripcion: "-", direccion: this.state.ubicacion, domicilio:this.state.precio_domicilio }
          console.log('la respuesta al crear el .map para agregar los carritos hijos',ordenFinal, 'index===>', index);
          this.addChildCar(orden, ordenFinal);
        });
        // localStorage.removeItem('ordenes');
      })
      .catch(err => console.error("error al crear la orden", err));
  };

  addChildCar = (data, ordenFinal) => {
    const { token } = this.state;
    const id = data._id;
    console.log('esta es toda la data que recibe cuando va agregar un carrito hijo');
    console.log('id', data._id);
    console.log('esta es la orden completa que recibe addChildCar =>',ordenFinal);
    console.log(JSON.stringify(ordenFinal));
    fetch(`${url}gst-ordenes/agregar-carrito-a-orden/${id}`,{
      method:'POST',
      headers: {
        'Content-Type':'application/json',
        Authorization: token
      },
      body :JSON.stringify(ordenFinal)
    })
    .then(res =>res.json())
    .then(res =>console.log('respuesta al crear los carrtios hijos =>', res))
    .catch(err=>console.error('error al agregar carritos hijos', err));
  };

  calcularDomicilio = (data) => {
    const price_domicilio = data;
    const { token } = this.state;
    console.log("calculo domicilio" , price_domicilio);
    console.log("JSON STRINNFY",JSON.stringify(price_domicilio) );
    fetch(`${url}gst-ordenes/calculate`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      },
      body : JSON.stringify(data)

    })
    .then((res => res.json()))
    .catch(error => console.error('Error:', error))
    .then(response => {
      console.log('Success:', response.domicilio);
      this.setState({precio_domicilio: response.domicilio})
  });


  };



  render() {
    return (
      <div className="container py-5 pl-5">
        <Row className="d-flex justify-content-between">
          <div className="col-6">
            <p>
              <strong>Dirección de envio</strong>
            </p>
            <p>___________</p>
            <br />
            <label
              className="h3-responsive d-flex justify-content-center"
              for=""
            >
              En que parte de la U estas?
            </label>
            <p className="grey-text d-flex justify-content-center">
              (se lo mas especifico posible)
            </p>
            <Input
              type="textarea"
              label="Descripción"
              icon="pencil"
              value={this.state.ubicacion}
              onChange={event =>
                this.setState({ ubicacion: event.target.value })
              }
            />
          </div>
          <div className="col-6 d-flex justify-content-start">
            <View className="detalles p-4">
              <p>
                <strong>Order Detalls</strong>
              </p>
              <p>___________</p>
              <div className="row d-flex justify-content-between">
                <div className="col-md-2">
                  <p className="pl-4">
                    <font size="2">
                      <b>Productos</b>
                    </font>
                  </p>
                </div>
                <div className="col-md-8 pt-5">
                  <hr />
                  {Array.isArray(this.state.ordenCompleta) &&
                    this.state.ordenCompleta.productos.map(orden => {
                      console.log("primer map", orden);
                      return orden.productos.map((item, index, array) => {
                        console.log("segundo map", item);
                        return (
                          <div className="row">
                            <div className="col-10">
                              <p>
                                <b>{item.cantidad}x </b> {item.nombre_producto}
                              </p>
                            </div>
                            <div className="col-2">
                              <b>${item.total}</b>
                            </div>
                          </div>
                        );
                      });
                    })}

                  <hr />
                  <div className="row d-flex justify-content-between">
                    <div className="col-3">
                      <p>
                        <b>Subtotal</b>
                      </p>
                    </div>
                    <div className="col-3">
                      <b>{`$${this.state.precioTotal}`}</b>
                    </div>
                  </div>
                  <hr />
                  
                  <div className="row d-flex justify-content-between">
                  <div className="col-3">
                      <p>
                        <b>Precio Domicilio</b>
                      </p>
                    </div>
                    <div className="col-3">
                      <b>{`$${this.state.precio_domicilio}`}</b>
                    </div>
                  </div>
                  <hr />

                  <div className="row">
                    <div className="col-3">
                      <p>
                        <b>Delivery</b>
                      </p>
                    </div>
                    <div className="col-9 align-items-end">
                      <p className="grey-text">Free Delivery on Mondays</p>
                    </div>
                  </div>
                  <hr />
                  <div className="row d-flex justify-content-between">
                    <div className="col-6">
                      <p>
                        <b>Order Total</b>
                      </p>
                    </div>
                    <div className="col-3">
                      <b>{`$${this.state.precioTotal + this.state.precio_domicilio}`}</b>
                    </div>
                  </div>
                </div>
                <div className="col-md-2" />
              </div>
              <div className="text-center">
                <Input
                  gap
                  onClick={this.onClick4}
                  checked={this.state.radio2 === 4 ? true : false}
                  label="Efectivo"
                  type="radio"
                  id="radio4"
                />
                <p>
                  <b>Con cuanto pagas?</b>
                </p>
                <p className="grey-text">
                  Selecciona las denominaciones de los billetes, puedes
                  seleccionar varias veces una denominación.
                </p>
                <Button outline className="badge-pill px-3">
                  1.000
                </Button>
                <Button outline className="badge-pill px-3">
                  2.000
                </Button>
                <Button outline className="badge-pill px-3">
                  5.000
                </Button>
                <Button outline className="badge-pill px-3">
                  10.000
                </Button>
                <Button outline className="badge-pill px-3">
                  20.000
                </Button>
                <Button outline className="badge-pill px-3">
                  50.000
                </Button>
                {/* <Button outline className="badge-pill px-3">
                  Exacto
                </Button> */}
                <Input
                  gap
                  onClick={this.onClick5}
                  checked={this.state.radio2 === 5 ? true : false}
                  label="Tarjeta de Credito"
                  type="radio"
                  id="radio5"
                />
                <Input label="Card number" />
                <div className="row px-3">
                  <div className="col-3">
                    <Input label="MM" />
                  </div>
                  <div className="col-3 px-3">
                    <Input label="YY" />
                  </div>
                  <div className="col-4">
                    <Input label="CCV" />
                  </div>
                </div>
              </div>
              <div className="text-center">
                <Button
                  onClick={() => this.confirmOrder()}
                  href="#"
                  className="badge-pill px-4"
                  color="danger"
                >
                  Confirmar Pedido
                </Button>
              </div>
            </View>
          </div>
        </Row>
      </div>
    );
  }
}

export default Contenido;
