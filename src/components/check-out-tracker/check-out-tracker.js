import React, { Component } from 'react';
import  {Stepper, Container, Row, Col, Button, Modal, ModalBody, ModalFooter, ModalHeader, View } from 'mdbreact';
import './check-out-tracker.css';
import Steps, { Step } from 'rc-steps';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';
import reloj from '../../img/icons/reloj.png'
import Foot from '../foot/foot';
import Imagenes from '../imagenes/imagenes';
import ModalPage from './modal';
import urunner from '../../img/icons/urunner.png'
import logo from '../../img/logo-blanco.png'
import { fire, message } from '../../config/firebase';
import { BrowserRouter, Redirect , Route, Switch, Link } from "react-router-dom";



class Tracker extends Component {


    constructor(props) {
        super(props);
        this.state = {
          modal: false,
          paso: 0,
          idCarritoHijo: null,
          idCarritoPadre: null,
          menssages: [],
          text:'',
          id: null
        };
    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

      getMessageRef = () => {
          const { idCarritoHijo }= this.state;
        return fire.database().ref(`chats/${idCarritoHijo}`);

      }


    handleSend = () => {
        // const { photoURL,uid } = firebase.auth().currentUser;
        const { text } = this.state;
        // const id_user = this.props.navigation.getParam('id_runner');
        //necesitamos el id del usuario
        
        const artistComentRef = this.getMessageRef();
        const newComment = artistComentRef.push();
        newComment.set({
        text,
        // id_user
        });
        this.setState({text:''})
    }

    handleChangeText = text => this.setState({text})


      addComents = data => {
        const msg = data.val();
        const { menssages } = this.state;
        this.setState({
          menssages: menssages.concat(msg)
        });
      }
    
    /*  getDataUser = () => {
         const myInit= {
             method: 'GET',
             header: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             mode: 'cors',
             cache: 'default'          
            
            };

            let request = new Request('token',myInit);

            fetch(request)
            .then((response) => {
                return response.blob;

            })
            .then(())

     }*/

      componentDidMount = async ()=>{
        // const { PedidoPadre, idHijo } = this.props.location.params;
        console.log(this.props.location.params);
        // console.log("========================");
        // console.log("id padre: ", PedidoPadre);
        // console.log("id padre: ", idHijo);
        console.log("========================");
        const token = await localStorage.getItem('token');
        const id_cliente = await localStorage.getItem('idUser');


        const idHijo = await localStorage.getItem('idCarritoHijo');
        const carritoPadre = await localStorage.getItem('carritoPadre');

         console.log('ESTE ES EL TOKEN',token);
         console.log('ESTE ES EL IDuser',id_cliente);

        console.log('id hijo', idHijo, 'carrito padre', JSON.parse(carritoPadre)._id);
        this.setState({idCarritoHijo: idHijo, idCarritoPadre: JSON.parse(carritoPadre)._id },()=>{
            
        fire.database().ref(`orders/${this.state.idCarritoPadre}`).on("value", this.stateFirebase, err => console.log(err));
        })


        this.getMessageRef().on('child_added',this.addComents);

      }
      stateFirebase = data => {
          const { idCarritoHijo} = this.state;
        let position =1;
        // padre 5c18ff68f980b267675753f0 
        // hijo 5c18ff69f980b267675753f1
        const val = data.val();
        console.log("----------------------");
        console.log("data conseguido: ", data);
        console.log("----------------------");
        console.log("----------------------");
        console.log("val conseguido: ", val);
        console.log("----------------------");
        val['procesos'].map( filtro => {
            console.log('soy el filtro: ', filtro);
            console.log('soy el hijo: ', idCarritoHijo);
        if(filtro.id == idCarritoHijo){
            position= filtro.state
            if(position==6){
                // alert("el pedido ha sido terminado");
                // location.reload('/calificacion')
                 window.location.pathname='/calificacion';
                // position ++;
            }
        }
        } );    

        console.log('position: ', position );
        this.setState({ paso: position });
    }
  
    render() {
      return(

        <div style={{marginTop:150}}>
                <div className="container-fluid pt-5">
                    <ul className="breadcrumb bg-pagnav px-5" style={{backgroundColor:'white'}}>
                        <li className="breadcrumb-item"><a href="/home" style={{color:'black'}}>Home</a></li>
                        <li className="breadcrumb-item"><a href="/#" style={{color:'black'}}>Shop</a></li>
                        <li className="breadcrumb-item active">Food Tracker</li>
                    </ul>
                </div>
                 <hr/>
            <div className="container px-5 py-4">
                <View className="bggreen1 text-center">
                    <Row>
                        <Col md="5 d-flex justify-content-end">
                            <img src={reloj} height="100" className="img-fuid"/>
                        </Col>
                        <Col md="7 text-left">
                            <p className="h3-responsive grey-text">Estado de tu Orden:</p>
                            <p className="h4-responsive"><strong>Un URunner ya fue asignado a tu ..... confirmando la existencia de tu pedido.</strong></p>
                        </Col>
                    </Row>
                    
                </View>

                <Steps step={1} steps={['First', 'Second', 'Third', 'Fourth']} />
                <br/><br/>
                <hr />
                <View className="tu pedidopy-5">
                <Steps labelPlacement="vertical" current={this.state.paso}>
                    <Step title="Orden recibida"  />
                    <Step title="Buscando URunner"  />
                    <Step title="Confirmando existencia"  />
                    <Step title="Preparando"  />
                    <Step title="En camino"  />
                    <Step title="Termiando"  />
                 </Steps>
                </View>
                 <View>
                     <Row>
                         <Col md="12" className="d-flex justify-content-end">
                            <img src={urunner} height="100" onClick={this.toggle} className />
                         </Col>
                     </Row>
                 </View>
                
                <Modal isOpen={this.state.modal} toggle={this.toggle} side position="top-right">
                <ModalHeader toggle={this.toggle} className="bgheader"><img src={logo} height="200" /></ModalHeader>
                <ModalBody className="modalBody">


                    
                </ModalBody>
                <ModalFooter className="modalHeader">
                    <div className="contentinput">
                            <input type="text" placeholder="Escribir un mensaje" className="inputChat"/>
                            <input type="submit" value="Enviar"/>
                    </div>
                </ModalFooter>
                </Modal>
            </div>
            {/* <Imagenes/> */}
            <Foot/>
        </div>
            
      );
    };
  }

  export default Tracker;