import React, { Component } from 'react';
import  {Button, View, Fa, Row, col } from 'mdbreact';
import { Collapse, CardBody, Card } from 'reactstrap';

import './reclutamiento.css';
import triste from '../../img/icons/triste.png'
import Foot from '../foot/foot';
import Navegation from './navegation';
import urunner from '../../img/icons/urunner.png'


class Contenido extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = { collapse: false };
      }
    
      toggle() {
        this.setState({ collapse: !this.state.collapse });
      }

      
    render() {
      return(
       
                <div className="container text-center">
                    <View className="pb-5">
                        <div className="row d-flex justify-content-center">
                            <div className="col-3">
                            </div>
                            <div className="col-6 text-center">
                                <p className="h2-responsive">Trabaja con Nosotros</p>
                                <p>____________</p>
                                <p className=""><b>Llena el formulario con los siguientes datos <br/>y alguien de UFood se pondrá en contacto contigo</b></p>
                                
                            </div>
                            <div className="col-3">
                            </div>                        
                        </div>
                        {/* Formulario */}
                        <div className="row d-flex justify-content-between text-left pt-5">
                            <div className="col-5">
                                <label for="nombre"><strong>Nombre completo</strong></label>
                                <input type="text" id="nombre" name="nombre" className="form-control" style={{borderRadius:'50px'}} placeholder="" value="" onChange="" />
                                <br/>
                                <label for="cedula"><strong>Cedula</strong></label>
                                <input type="text" id="cedula" name="cedula" className="form-control" style={{borderRadius:'50px'}} placeholder="" value="" onChange="" />
                                <br/>
                                <label for="codigo"><strong>Codigo Universitario</strong></label>
                                <input type="text" id="codigo" name="codigo" className="form-control" style={{borderRadius:'50px'}} placeholder="" value="" onChange="" />
                                <br/>
                                <label for="edad"><strong>Edad</strong></label>
                                <input type="text" id="edad" name="edad" className="form-control" style={{borderRadius:'50px'}} placeholder="" value="" onChange="" />
                                <br/>
                                    <label for="universidad"><strong>Universidad</strong></label>
                                        <select className="browser-default style-select form-control badge-pill" >
                                            <option value="" disabled selected></option>
                                            <option value="autonoma">Autonoma</option>
                                            <option value="icesi">Icesi</option>
                                            <option value="univalle">Univalle</option>
                                            <option value="javeriana">Javeriana</option>
                                        </select>
                                   <br/>
                                   <label for="semestre"><strong>Semestre</strong></label>
                                        <select className="browser-default style-select form-control badge-pill" value="" onChange="" style={{borderRadius:'50px'}}>
                                            <option value="" disabled selected></option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>                                
                                   
                                    <br/>
                                    <label for="carrera"><strong>Carrera</strong></label>
                                    <input type="text" id="carrera" name="carrera" className="form-control" style={{borderRadius:'50px'}} placeholder="" value="" onChange="" />
                                    <br/>
                            </div>
                        {/*Fin Formulario */}
                            
                        {/* Horas libres */}
                            <div className="col-6 text-center">
                            <p><b>Horas Libres</b></p>
                            <p className="grey-text"><em>Agrega los bloques de horas que tienes libres <br/>para trabjar con nosotros por días</em></p>
                                <div>
                                    <Row className="d-flex justify-content-between">
                                        <Col-3>
                                            <p className="h3-responsive">Lunes</p>
                                        </Col-3>
                                        <Col-4 className="">
                                            <a><Fa icon="chevron-down fa-2x" onClick={this.toggle} style={{color:'#ff3e44'}} className="mr-4"/></a>
                                        </Col-4>
                                    </Row>
                                    <Collapse isOpen={this.state.collapse}>
                                    <Card>
                                        <CardBody>
                                        Anim pariatur cliche reprehenderit,
                                        enim eiusmod high life accusamus terry richardson ad squid. Nihil
                                        anim keffiyeh helvetica, craft beer labore wes anderson cred
                                        nesciunt sapiente ea proident.
                                        <p><hr/></p>
                                        </CardBody>
                                    </Card>
                                    </Collapse>
                                </div>
                            </div>                        
                        </div>                        

                    </View>
                            <Button className="badge-pill px-5" color="danger">Enviar</Button><br/>        
                </div>
           
            
      );
    };
  }

  export default Contenido;