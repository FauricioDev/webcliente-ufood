import React, { Component } from 'react';
import  {Button, View, Row, Col } from 'mdbreact';
import './reclutamiento.css';
import triste from '../../img/icons/triste.png'
import Foot from '../foot/foot';
import Navegation from './navegation';
import Contenido from './contenido';
import urunner from '../../img/icons/urunner.png'


class Reclutamiento extends Component {
   
    render() {
      return(
       
        <div style={{marginTop:150}}>
                <Navegation/>
                <Contenido/>
                <Foot/>
        </div>
            
      );
    };
  }

  export default Reclutamiento;