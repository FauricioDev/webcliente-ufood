import React, { Component } from "react";
import {
  Navbar,
  NavbarBrand,
  NavbarNav,
  NavbarToggler,
  Collapse,
  NavItem,
  NavLink,
  Container,
  Mask,
  View,
  Col,
  Carousel,
  CarouselCaption,
  CarouselInner,
  CarouselItem,
  Row,
  Nav,
  Fa,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "mdbreact";
import { BrowserRouter as Router, Link } from "react-router-dom";
import logo from "../../img/logo-blanco.png";
import slide1 from "../../img/01.jpg";
import slide2 from "../../img/02.jpg";
import slide3 from "../../img/03.jpg";
import googlePlay from "../../img/google-play.png";
import AppStore from "../../img/app-store.png";
import classnames from "classnames";
import {
  Tabs,
  Tab,
  TabContainer,
  TabContent,
  TabPane,
  NavDropdown,
  MenuItem
} from "react-bootstrap";
import "./navegacion.css";

//importaciones redux
import { connect } from "react-redux";

class Navegacion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      isWideEnough: false,
      activeItem: "1"
    };
    this.onClick = this.onClick.bind(this);
  }
  onClick() {
    this.setState({
      collapse: !this.state.collapse
    });
  }
  componentDidMount = async () => {};
  render() {
    const view = { height: "100vh" };
    const logged = localStorage.getItem("userToken");
    console.log(logged);
    console.log("hola desde la navegacion");
    const orden = localStorage.getItem("ordenCompleta");
    console.log(
      "esta es la respuesta de la orden desde la navegacion",
      JSON.parse(orden)
    );
    return (
      <div>
        <header>
          <Navbar dark fixed="top" expand="lg" scrolling>
            {/* <Navbar inverse color="transparent" dark fixed="top" expand="md"> */}
            {!this.state.isWideEnough && (
              <NavbarToggler onClick={this.onClick} />
            )}
            <Collapse isOpen={this.state.collapse} navbar>
              <NavbarNav className="align-items-center text-center">
                      {logged == null ? (
                  <div style={{display: 'flex', flexDirection: 'row', justifyContent:'space-around', width: '80%'}}>
                    <NavItem>
                      <NavLink to="/login"
                      onClick={()=>window.reload('/')}
                      >Login</NavLink>
                    </NavItem>
                <NavbarBrand href="/home">
                  <img
                    src={logo}
                    height="120"
                    className="d-inline-block align-top"
                  />
                </NavbarBrand>
                    <NavItem>
                      <NavLink to="/registro"
                      onClick={()=>window.reload('/')}
                      >Register</NavLink>
                    </NavItem>
                  </div>
                ):
                <div style={{display: 'flex', flexDirection: 'row', justifyContent:'space-around', width: '100%'}}>
                <NavItem
                    style={{width:200,flexDirection: 'row', display: 'flex',}}
                >
                {/* <Link to="/Check-out-paso-1"> */}
                                  <NavLink to="/Check-out-paso-1">

                  <span
                    className="fa-stack fa-lg"
                    onClick={() => console.log("holis")}
                  >
                    <i
                      className="fa fa-circle-thin fa-stack-2x"
                      style={{ color: "white" }}
                    />
                    <NavItem style={{ width:200 }}>
                    <i
                      className="fa fa-shopping-cart fa-md fa-stack-1x"
                      aria-hidden="true"
                      style={{ color: "white" }}
                    />
                                      {/* <NavLink to=""> */}
                                      {/* </NavLink> */}

                     {/* <p>Ver carrito</p> */}
                    </NavItem>
                  </span>
                    <h4> Ver Carrito </h4>
                {/* </Link> */}
                </NavLink>
              </NavItem>
                <NavbarBrand href="/home">
                  <img
                    src={logo}
                    height="120"
                    className="d-inline-block align-top"
                  />
                </NavbarBrand>
                <NavItem>
                  <NavLink to="/Pedidos">Tus pedidos</NavLink>
                </NavItem>
                <NavItem 
                >
                      <NavLink to="/login"
                       onClick={()=>{
                        localStorage.removeItem('userToken');
                        this.props.history.push('/');                   
                        window.location.reload();
    
                      }
                    }
                      >Salir</NavLink>
                    </NavItem>
                    </div>
                }

                <NavItem>
                  <font color="white">Descarga Nuestra App Cliente</font>
                  <br />
                  <img src={googlePlay} height="50" />
                  <br />
                  <img src={AppStore} height="50" />
                </NavItem>
              </NavbarNav>
            </Collapse>
          </Navbar>
          <View style={view}>
            <Carousel
              activeItem={1}
              length={3}
              showControls={true}
              showIndicators={true}
              className="z-depth-1"
            >
              <CarouselInner>
                <CarouselItem itemId="1">
                  <View>
                    <img
                      className="d-block w-100"
                      height="600"
                      src={slide1}
                      alt="First slide"
                    />
                    <Mask overlay="black-strong" />
                  </View>
                  <CarouselCaption className="pb-5 mb-5">
                    <p className="h1 font-weight-normal">
                      .......
                      <span className="bggreen">
                        <font size="4">$</font>12<font size="4">.000</font>
                      </span>
                      .......
                    </p>
                    <p className="h1-responsive font-weight-bold text-uppercase">
                      ORIGINAL DE USA
                    </p>
                    <p className="h4-responsive font-weight text-uppercase">
                      Combo hamburguesa-Amantes{" "}
                    </p>
                  </CarouselCaption>
                </CarouselItem>
                <CarouselItem itemId="2">
                  <View>
                    <img
                      className="d-block w-100"
                      height="600"
                      src={slide2}
                      alt="Second slide"
                    />
                    <Mask overlay="black-strong" />
                  </View>
                  <CarouselCaption className="pb-5 mb-5">
                    <p className="h1 font-weight-normal">
                      .......
                      <span className="bggreen">
                        <font size="4">$</font>12<font size="4">.000</font>
                      </span>
                      .......
                    </p>
                    <p className="h1-responsive font-weight-bold text-uppercase">
                      ORIGINAL DE USA
                    </p>
                    <p className="h4-responsive font-weight text-uppercase">
                      Combo hamburguesa-Amantes{" "}
                    </p>
                  </CarouselCaption>
                </CarouselItem>
                <CarouselItem itemId="3">
                  <View>
                    <img
                      className="d-block w-100"
                      height="600"
                      src={slide3}
                      alt="Third slide"
                    />
                    <Mask overlay="black-strong" />
                  </View>
                  <CarouselCaption className="pb-5 mb-5">
                    <p className="h1 font-weight-normal">
                      .......
                      <span className="bggreen">
                        <font size="4">$</font>12<font size="4">.000</font>
                      </span>
                      .......
                    </p>
                    <p className="h1-responsive font-weight-bold text-uppercase">
                      ORIGINAL DE USA
                    </p>
                    <p className="h4-responsive font-weight text-uppercase">
                      Combo hamburguesa-Amantes{" "}
                    </p>
                  </CarouselCaption>
                </CarouselItem>
              </CarouselInner>
            </Carousel>
          </View>
        </header>
      </div>
    );
  }
}

// export default Navegacion;
const mapStateToProps = (state, props) => {
  console.log("soy el state", state);
  console.log("soy los props", props);
  return {
    dataUser: state.login
  };
};

// const mapDispatchToProps = {
// // getApiLoginAsync,
// // getApiLogin
// };

export default connect(
  mapStateToProps,
  null
)(Navegacion);
