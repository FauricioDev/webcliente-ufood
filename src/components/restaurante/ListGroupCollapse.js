import React from 'react';
import { ListGroupItem, Collapse } from 'reactstrap';

class ListGroupCollapse extends React.Component {
  constructor(props) {
    super(props);
    
    this.toggle = this.toggle.bind(this);
    this.state = {collapse: false};
  }
  
  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }
  
  render() {
    const cat = this.props.cat;
    let date = cat.createdAt.toISOString();
    date = date.substring(0, date.indexOf('T'));
    return (
      <ListGroupItem>
        <div>
          <p onClick={this.toggle}>
            <strong>{cat.title}<br/>
            </strong> - {date}
          </p>
          <Collapse isOpen={this.state.collapse}>{cat.info}</Collapse>
        </div>
      </ListGroupItem>
    );
  }
}

export default ListGroupCollapse