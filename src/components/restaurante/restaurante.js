import "./restaurante.css";
import React, { Component } from "react";
import { Button, Collapse, View, Row, Col, Fa } from "mdbreact";
import slide2 from "../../img/02.jpg";
import circulo from "../../img/circuloverde.png";
import ListGroupCollapse from "./ListGroupCollapse";
import { Container } from "reactstrap";
import Foot from "../foot/foot";
import url from "../../config/url";

import { BrowserRouter, Redirect, Route, Switch, Link } from "react-router-dom";
import { connect } from "react-redux";
import { ordenCreate } from '../../store/actions/orden';


const things = {
  idk: {
    createdAt: new Date(),
    title: "El corral",
    info: "aca va toda la informacion"
  },
  another: {
    createdAt: new Date("2010-10-10"),
    title: "El blablabla",
    info: "aca va toda la informacion2"
  },
  more: {
    createdAt: new Date("2011-11-11"),
    title: "El dddsssd",
    info: "aca va toda la informacion3"
  }
};

class Restaurante extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);

    this.state = {
      collapse: false,
      comerce: null,
      stateType: 0,
      ordenes: []
    };
  }
  componentDidMount = async () => {
    console.log("EL RESTAURANTE CON LA OPCION DE MENU");
    const token = await localStorage.getItem("userToken");
    console.log("soy token!", token);
    this.getAllRestaurants(token);
  };
  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  getAllRestaurants = token => {
    console.log("token: ", token, "url:", url);
    fetch(`${url}auth/users/list/4/`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: token
      }
    })
      .then(res => res.json())
      .then(comerce => {
        console.log("respuesta al traer todos los restaurantes", comerce);
        this.setState({ comerce });
      })
      .catch(e => console.error("error al traer los restaurantes", e));
  };

  toMenu = async (item, comerceType)=>{
    const ordenCompletaLocalStorage = await localStorage.getItem("ordenes");
    const orden = JSON.parse(ordenCompletaLocalStorage);
    console.log('-------------------------------------------------------',ordenCompletaLocalStorage,'--------------',orden )
    console.log('tipo de variable',typeof ordenCompletaLocalStorage);
    const body1 ={

            order_type: 1,
            productos: [],
            descripcion: '',
            direcion: '',
            fecha_creacion: new Date(),
            total: 0,
                  }
    if(comerceType=="restaurante"){
      console.log('if(comerceType=="restaurante"){');
      if(orden){
        console.log('if(orden){');
        if(orden.order_type  !=1){
          console.log('if(orden.orden_type  !==1){');
            localStorage.setItem('ordenes', JSON.stringify({...body1, stateType: 1}) );
            localStorage.setItem('stateType', "1")

            this.setState({stateType: 1},()=>{
              console.log("=======================");
              console.log("se acaba asignar el stateType en restaurante: ", this.state.stateType);
              console.log("=======================");
            })
                                  }
                                  localStorage.setItem('stateType', "1")
                                  this.setState({stateType: 1},()=>{
                                    console.log("=======================");
                                    console.log("se acaba asignar el stateType en restaurante: ", this.state.stateType);
                                    console.log("=======================");
                                  })
                                  
                }
    }else if(comerceType=="cafeteria"){
      console.log('}else if(comerceType=="cafeteria"){');
      // if(orden){
        console.log('if(orden){');
        if(orden.order_type !=1){
          console.log('if(orden.orden_type !==1){');
            localStorage.setItem('ordenes', JSON.stringify({...body1, stateType: 2}));
            // this.setState({stateType: 2},()=>{
            //   console.log("=======================");
            //   console.log("se acaba asignar el stateType en restaurante: ", this.state.stateType);
            //   console.log("=======================");
            // })
            localStorage.setItem('stateType', "2")
    }
    this.setState({stateType: 2},()=>{
      console.log("=======================");
      console.log("se acaba asignar el stateType en restaurante: ", this.state.stateType);
      console.log("=======================");
    })
    localStorage.setItem('stateType', "2")

                                  // }
    }else if(comerceType=="emprendimiento"){
      console.log('}else if(comerceType=="emprendimiento"){');
    if(orden){
      console.log('if(orden){');
        if(orden.order_type !=2){
          console.log('if(orden.orden_type !==2){');
            alert('estas a punto a un comercio tipo restaurante o cafeteria, si presionas algun')
            localStorage.setItem('ordenes', JSON.stringify({...body1, order_type: 2, stateType: 1}) );
            this.setState({stateType: 1}, ()=>{
              console.log("=======================");
              console.log("se acaba asignar el stateType en restaurante: ", this.state.stateType);
              console.log("=======================");
            })
            localStorage.setItem('stateType', "1")


        }
        this.setState({stateType: 1},()=>{
          console.log("=======================");
          console.log("se acaba asignar el stateType en restaurante: ", this.state.stateType);
          console.log("=======================");
        })
        localStorage.setItem('stateType', "1")

        }
    }
    // localStorage.setItem("idRestaurant", item._id);
                                          }
  
  render() {
    var comerceType = localStorage.getItem("comerceType");
    return (
      <div>
        <div className="container">
          <hr style={{ paddingTop: 50 }} />
          <p className="h1-responsive text-center font-weight-bold text-uppercase">
            {comerceType}
          </p>
          {Array.isArray(this.state.comerce) && (
            <div className="containerRestaurant">
              {this.state.comerce.map((item, index, array) => {
                if (item.comercetype == comerceType) {
                  console.log(item);
                  return (
                    <View
                      className="restaurante pb-3"
                      onClick={() => console.log(item._id)}
                    >
                      <div className="container-fluid text-center">
                        <img
                          src={slide2}
                          alt="01"
                          height="200"
                          className="rounder img-fluid"
                          onClick={this.toggle}
                          style={{ marginBottom: "1.5rem", marginTop: "1rem" }}
                        />
                        <p
                          className="h3-responsive font-weight-bold"
                          onClick={this.toggle}
                        >
                          {item.nombre}
                        </p>
                      </div>
                      <Collapse
                        isOpen={this.state.collapse}
                        className="text-center"
                      >
                        <div className="container">
                          <p
                            className="text-center font-italic"
                            style={{ color: "rgba(189, 189, 189, 0.958)" }}
                          >
                            texto texto texto texto texto texto texto texto
                            texto texto
                          </p>
                          <Row className="d-flex justify-content-center">
                            <Col>
                              <hr />
                            </Col>
                            <Col>
                              <p>Detalle</p>
                            </Col>
                            <Col>
                              <hr />
                            </Col>
                          </Row>
                          <Row className="px-1 d-flex justify-content-between">
                            <Col-4>
                              <img
                                src={circulo}
                                alt="abierto"
                                height="60"
                                className=""
                              />
                            </Col-4>
                            <Col-1>
                              <p className="">
                                <font size="1" sytle={{ color: "black" }}>
                                  Abre
                                </font>
                                <br />
                                <font size="1" sytle={{ color: "black" }}>
                                  9:00 AM
                                </font>
                                <br />
                                <font size="1" sytle={{ color: "black" }}>
                                  Cierra
                                </font>
                                <br />
                                <font size="1" sytle={{ color: "black" }}>
                                  10:00 PM
                                </font>
                              </p>
                            </Col-1>
                            <Col-7>
                              <p className="d-flex justify-content-end">
                                <font size="2" sytle={{ color: "black" }}>
                                  <br />
                                  {item.direccion}
                                </font>
                              </p>
                            </Col-7>
                          </Row>
                          <Link
                            to={{
                              pathname: "/perfil-restaurante",
                              params: { nombre: item.nombre, idRestaurant: item._id, statetype: this.state.stateType }
                            }}
                          >
                            <Button
                              color="danger"
                              size="md"
                              className="rounded"
                              onClick={() =>{
                                this.toMenu(item, comerceType)
                                console.log("=================================");
                                console.log("entro al metodo toMenu:");
                                console.log("=================================");

                              }
                              }
                            >
                              <Fa icon="clipboard" className="mr-1" />
                              Menu
                            </Button>
                          </Link>
                        </div>
                      </Collapse>
                    </View>
                  );
                }
              })}
            </div>
          )}

          {/* <Container className="py-4">
            <Row>
                <Col-3>
                    {Object.keys(things).map((key, index) =>
                    <ListGroupCollapse key={index} cat={things[key]} />
                    )}
                </Col-3>
            </Row>
        </Container> */}
        </div>
        <Foot />
      </div>
    );
  }
}
// export default Restaurante;

const mapStateToProps = (state, props) => {
  console.log('los estados de redux desde la pantalla de restaurante', state);
  return {
    orden: state.orden.orden
  };
};
const mapDispatchToProps = {
  ordenCreate
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Restaurante);