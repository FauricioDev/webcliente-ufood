import '../restaurante.css'
import React, { Component } from 'react';
import { Button, Collapse, View, Row, Col, Fa } from 'mdbreact';
import slide2 from '../../../img/02.jpg'
import circulo from '../../../img/circuloverde.png'
import { Container } from 'reactstrap';

export const CardViewRestaurant = (props)=>{

    const things = {
        idk: {
            createdAt: new Date(),
            title: 'El corral',
            info: 'aca va toda la informacion'
        },
        another: {
          createdAt: new Date('2010-10-10'),
          title: 'El blablabla',
        info: 'aca va toda la informacion2'
        },
        more: {
          createdAt: new Date('2011-11-11'),
          title: 'El dddsssd',
            info: 'aca va toda la informacion3'
        }
      }
      
    return (
        <View className="restaurante pb-3">
        <div className="container-fluid text-center">
            <img src={slide2} alt="01" height="200" className="rounder img-fluid" onClick={this.toggle} style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                <p className="h3-responsive font-weight-bold" onClick={this.toggle}>El corral</p>
        </div>
        <Collapse isOpen={props.stateCollapse} className="text-center">
            <div className="container">
                <p className="text-center font-italic" style={{color: 'rgba(189, 189, 189, 0.958)'}}>texto texto texto texto texto texto texto texto texto texto</p>
                <Row className="d-flex justify-content-center">
                    <Col><hr/></Col>
                    <Col><p>Detalle</p></Col>
                    <Col><hr/></Col>
                </Row>
                <Row className="px-1 d-flex justify-content-between">
                    <Col-4>
                    <img src={circulo} alt="abierto" height="60" className="" />
                    </Col-4>
                    <Col-1>
                        <p className="">
                            <font size="1" sytle={{color:'black'}}>Abre</font>
                            <br/>
                            <font size="1" sytle={{color:'black'}}>9:00 AM</font>
                            <br/>
                            <font size="1" sytle={{color:'black'}}>Cierra</font>
                            <br/>
                            <font size="1" sytle={{color:'black'}}>10:00 PM</font>
                        </p>
                    </Col-1>
                    <Col-7>
                        <p className="d-flex justify-content-end">
                            <font size="2" sytle={{color:'black'}}>Carrera 105 # 45-87 <br/>
                            Ciudad Jardin
                            </font>
                        </p>
                    </Col-7>
                </Row>

                <Button color="danger" size="md" className="rounded"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
            </div>
        </Collapse>
    </View>
    );
}

export default CardViewRestaurant;