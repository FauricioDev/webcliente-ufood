import React, { Component } from 'react';
import  {Carousel, CarouselCaption, CarouselInner, CarouselItem, View, Mask, Container, Row, Col, CardGroup, Card, CardText, CardBody, CardImage, CardTitle, Button, Footer, Table, Input, Fa } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import { database } from '../../config/firebase';
import './home.css';
import mcdonalds from '../../img/mcdonalds.png';
import subway from '../../img/subway.png';



class Section1 extends Component {
    componentDidMount = () => {
    this.testFirebase().on('value', snap => console.log('snap =============>>>>>>>', snap.val()))
}

testFirebase = () => database.ref('orders')
  render(){
    return(
      <div className="container-fluid py-4 justify-content-center" style={{backgroundColor: '#E6E6E6', marginTop:120}}>
        <h4 className="mt-5 mb-2"></h4>
        <Carousel
          activeItem={1}
          length={3}
          showControls={true}
          showIndicators={true}
          className="pb-5"
          >
          
          <CarouselInner>
            <CarouselItem itemId="1">
              <View>
              <CardGroup deck className="mb-4">
                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/16.jpg" height="286" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">El paso</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>

                    <Card>
                        <CardImage src={subway} alt="Card image cap" height="286" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Subway!</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>

                    <Card>
                        <CardImage src={mcdonalds} alt="Mc Donald's" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Mc Donalds</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>
                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/15.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Mr Wings</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>
                </CardGroup>
                
              </View>
              
            </CarouselItem>
            <CarouselItem itemId="2">
              <View>
              <CardGroup deck className="mb-4">
                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/16.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Burger King</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>

                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/14.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Panel title</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>

                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/15.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Panel title</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>
                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/15.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Panel title</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>
                </CardGroup>
              </View>
              
            </CarouselItem>
            <CarouselItem itemId="3">
              <View>
              <CardGroup deck className="mb-4">
                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/16.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Panel title</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>

                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/14.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Panel title</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>

                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/15.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Panel title</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>
                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/15.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="text-center">
                            <CardTitle tag="h5">Panel title</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Menu</Button>
                        </CardBody>
                    </Card>
                </CardGroup>
              </View>
              
            </CarouselItem>
           
          </CarouselInner>
        </Carousel>
      </div>


    );
  }
}

export default Section1;