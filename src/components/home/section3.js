import React, { Component } from 'react';
import  {Carousel, CarouselCaption, CarouselInner, CarouselItem, View, Mask, Container, Row, Col, CardGroup, Card, CardText, CardBody, CardImage, CardTitle, Button, Footer, Table, Input, Fa } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import './home.css'
import slide2 from '../../img/02.jpg'





class Section3 extends Component {
    
      
  render(){
    return(
       
      <View className="container">
        
        <View className="row">

                <Tabs contentClass="tab-content" headerClass="tab-header-bold" onSelect={(index, label) => console.log(`Selected Index: ${index}, Label: ${label}`)} >
                    <Tab label="Hamburguesa" className="Hamburguesa-content">
                            <CardGroup deck className="mb-4 py-4">
                                    <Card className="taab">
                                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/16.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                                        <CardBody className="text-center">
                                            <CardTitle tag="h5">Supreme Pizza</CardTitle>
                                            <CardText>
                                                <View className="container">
                                                    <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                    <Row className="d-flex justify-content-between">
                                                        <Col><hr/></Col>
                                                        <Col><p>Pick Size</p></Col>
                                                        <Col><hr/></Col>
                                                    </Row>
                                                    <Row className="px-1 d-flex justify-content-center">
                                                        <Col-3 className="">
                                                            <span className="fa-stack fa-lg">
                                                                <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                            </span>                                                        
                                                        </Col-3>
                                                        <Col-3 className="">
                                                            <p className="px-4 pt-2">
                                                                <font size="1" sytle={{color:'black'}} className="">$</font>
                                                               19.90
                                                            </p>
                                                        </Col-3>
                                                        <Col-3 className="">
                                                            <span className="fa-stack fa-lg">
                                                                <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                            </span>
                                                        </Col-3>
                                                        <Col-3>
                                                            <p className="px-4 pt-2">
                                                                <font size="1" sytle={{color:'black'}} className="">$</font>
                                                               19.90
                                                            </p>
                                                        </Col-3>
                                                    </Row>
                                                </View>
                                            </CardText>
                                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                        </CardBody>
                                    </Card>

                                    <Card>
                                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/14.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                                        <CardBody className="text-center">
                                            <CardTitle tag="h5">Panel title</CardTitle>
                                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                        </CardBody>
                                    </Card>

                                    <Card>
                                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/15.jpg" alt="Card image cap" top hover overlay="white-slight"/>
                                        <CardBody className="text-center">
                                            <CardTitle tag="h5">Panel title</CardTitle>
                                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                            <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                        </CardBody>
                                    </Card>
                                    
                            </CardGroup>
                    </Tab>
                    <Tab label="Pizza" className="Pizza-content">
                        <Row className="p-3">
                        <div className="col-sm-12 col-md-4">
                                    <View className="home">
                                        <div className="container-fluid text-center">
                                            <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                                                <p className="h3-responsive font-weight-bold">Subway</p>
                                        </div>
                                        <div className="container">
                                            <CardBody className="text-center">
                                                <CardTitle tag="h5">Supreme Pizza</CardTitle>
                                                <CardText>
                                                    <View className="container">
                                                        <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                        <Row className="d-flex justify-content-between">
                                                            <Col><hr/></Col>
                                                            <Col><p>Pick Size</p></Col>
                                                            <Col><hr/></Col>
                                                        </Row>
                                                        <Row className="px-1 d-flex justify-content-center">
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                                </span>                                                        
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                                </span>
                                                            </Col-3>
                                                            <Col-3>
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                        </Row>
                                                    </View>
                                                </CardText>
                                                <Button className="badge-pill" color="danger" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                            </CardBody>
                                        </div>
                                    </View>
                                </div>
                                <div className="col-sm-12 col-md-4">
                                    <View className="home">
                                        <div className="container-fluid text-center">
                                            <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                                                <p className="h3-responsive font-weight-bold">Subway</p>
                                        </div>
                                        <div className="container">
                                            <CardBody className="text-center">
                                                <CardTitle tag="h5">Supreme Pizza</CardTitle>
                                                <CardText>
                                                    <View className="container">
                                                        <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                        <Row className="d-flex justify-content-between">
                                                            <Col><hr/></Col>
                                                            <Col><p>Pick Size</p></Col>
                                                            <Col><hr/></Col>
                                                        </Row>
                                                        <Row className="px-1 d-flex justify-content-center">
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                                </span>                                                        
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                                </span>
                                                            </Col-3>
                                                            <Col-3>
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                        </Row>
                                                    </View>
                                                </CardText>
                                                <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                            </CardBody>
                                        </div>
                                    </View>
                                </div>
                                <div className="col-sm-12 col-md-4">
                                    <View className="home">
                                        <div className="container-fluid text-center">
                                            <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                                                <p className="h3-responsive font-weight-bold">Subway</p>
                                        </div>
                                        <div className="container">
                                            <CardBody className="text-center">
                                                <CardTitle tag="h5">Supreme Pizza</CardTitle>
                                                <CardText>
                                                    <View className="container">
                                                        <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                        <Row className="d-flex justify-content-between">
                                                            <Col><hr/></Col>
                                                            <Col><p>Pick Size</p></Col>
                                                            <Col><hr/></Col>
                                                        </Row>
                                                        <Row className="px-1 d-flex justify-content-center">
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                                </span>                                                        
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                                </span>
                                                            </Col-3>
                                                            <Col-3>
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                        </Row>
                                                    </View>
                                                </CardText>
                                                <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                            </CardBody>
                                        </div>
                                    </View>
                                </div>
                            </Row>
                        </Tab>
                        <Tab label="Wraps" className="Wraps-content">
                            <Row className="p-3">
                            <div className="col-sm-12 col-md-4">
                                    <View className="home">
                                        <div className="container-fluid text-center">
                                            <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                                                <p className="h3-responsive font-weight-bold">Wraps</p>
                                        </div>
                                        <div className="container">
                                            <CardBody className="text-center">
                                                <CardTitle tag="h5">Supreme Wraps</CardTitle>
                                                <CardText>
                                                    <View className="container">
                                                        <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                        <Row className="d-flex justify-content-between">
                                                            <Col><hr/></Col>
                                                            <Col><p>Pick Size</p></Col>
                                                            <Col><hr/></Col>
                                                        </Row>
                                                        <Row className="px-1 d-flex justify-content-center">
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                                </span>                                                        
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                                </span>
                                                            </Col-3>
                                                            <Col-3>
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                        </Row>
                                                    </View>
                                                </CardText>
                                                <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                            </CardBody>
                                        </div>
                                    </View>
                                </div>
                                <div className="col-sm-12 col-md-4">
                                    <View className="home">
                                        <div className="container-fluid text-center">
                                            <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                                                <p className="h3-responsive font-weight-bold">Wraps 2</p>
                                        </div>
                                        <div className="container">
                                            <CardBody className="text-center">
                                                <CardTitle tag="h5">Supreme Pizza</CardTitle>
                                                <CardText>
                                                    <View className="container">
                                                        <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                        <Row className="d-flex justify-content-between">
                                                            <Col><hr/></Col>
                                                            <Col><p>Pick Size</p></Col>
                                                            <Col><hr/></Col>
                                                        </Row>
                                                        <Row className="px-1 d-flex justify-content-center">
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                                </span>                                                        
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                                </span>
                                                            </Col-3>
                                                            <Col-3>
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                        </Row>
                                                    </View>
                                                </CardText>
                                                <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                            </CardBody>
                                        </div>
                                    </View>
                                </div>
                                <div className="col-sm-12 col-md-4">
                                    <View className="home">
                                        <div className="container-fluid text-center">
                                            <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                                                <p className="h3-responsive font-weight-bold">Wraps 3</p>
                                        </div>
                                        <div className="container">
                                            <CardBody className="text-center">
                                                <CardTitle tag="h5">Supreme Pizza</CardTitle>
                                                <CardText>
                                                    <View className="container">
                                                        <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                        <Row className="d-flex justify-content-between">
                                                            <Col><hr/></Col>
                                                            <Col><p>Pick Size</p></Col>
                                                            <Col><hr/></Col>
                                                        </Row>
                                                        <Row className="px-1 d-flex justify-content-center">
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                                </span>                                                        
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                                </span>
                                                            </Col-3>
                                                            <Col-3>
                                                                <p className="px-4 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                        </Row>
                                                    </View>
                                                </CardText>
                                                <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                            </CardBody>
                                        </div>
                                    </View>
                                </div>
                            </Row>
                        </Tab>
                </Tabs>
        </View>
        <hr/>
        
    </View>
    

    );
  }
}

export default Section3;