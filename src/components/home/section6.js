import React, { Component } from 'react';
import  {Carousel, CarouselCaption, CarouselInner, CarouselItem, View, Mask, Container, Row, Col, CardGroup, Card, CardText, CardBody, CardImage, CardTitle, Button, Footer, Table, Input, Fa } from 'mdbreact';
import './home.css'

class Section6 extends Component {
    
      
  render(){
    return(
       

        <View className="fluid-container">
            <View className="bgbanner" >
                <Mask overlay="black-strong"></Mask>
                <Row className="d-flex justify-content-center">
                    <Col md="10 text-center text-uppercase py-5">
                        <p className="h3-responsive text-uppercase font-weight text-warning">Menu desayunos mc donals</p>
                    </Col>
                </Row>
                <Row className="d-flex justify-content-end px-5 py-5">
                    <Col md="3 text-uppercase py-5">
                        <Row className="12">
                            <Col md="10 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">ham & cheese<br/>omelette</p>
                                <font size="1" className="font-italic">Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly.</font>
                            </Col>
                            <Col md="2 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">9<font size="2">95</font></p>
                            </Col>
                        </Row>
                        <Row className="12 pt-4">
                            <Col md="10 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">loaded veggie<br/>omelette</p>
                                <font size="1" className="font-italic">Tender chicken, onion, capsicum and stretchy mozzarella, topped off with an apricot swirl.</font>
                            </Col>
                            <Col md="2 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">16<font size="2">95</font></p>
                            </Col>
                        </Row>
                        <Row className="12 pt-4">
                            <Col md="10 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">steak & eggs</p>
                                <font size="1" className="font-italic">Mouth watering pepperoni, cabanossi, mushroom, capsicum, black olives and stretchy mozzarella, seasoned with garlic and oregano.</font>
                            </Col>
                            <Col md="2 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">5<font size="2">95</font></p>
                            </Col>
                        </Row>
                    </Col>
                    
                    <div className="vl"></div>

                    <Col md="3 text-uppercase py-5">
                        <Row className="12 pr-4">
                            <Col md="10 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">peanut butter cup<br/>pancake</p>
                            </Col>
                        </Row>
                        <Row className="12 pr-5">
                            <Col md="10 text-uppercase">
                                <p className="h6-responsive text-uppercase font-weight">with cheese</p>
                            </Col>
                            <Col md="2 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">4<font size="2">95</font></p>
                            </Col>
                        </Row>
                        <Row className="12 pr-5">
                            <Col md="10 text-uppercase">
                                <p className="h6-responsive text-uppercase font-weight">with double cheese</p>
                            </Col>
                            <Col md="2 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">5<font size="2">95</font></p>
                            </Col>
                        </Row>
                        <Row className="12 pr-5 pt-4">
                            <Col md="10 text-uppercase">
                                <p className="h6-responsive text-uppercase font-weight text-warning">hearty breakfast<br/>skillet</p>
                                <font size="1" className="font-italic">Tender chicken, onion, capsicum and stretchy mozzarella, topped off.</font>
                            </Col>
                            <Col md="2 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">12<font size="2">95</font></p>
                            </Col>
                        </Row>
                        <Row className="12 pr-5 pt-5">
                            <Col md="10 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">bruschetta</p>
                                <font size="1" className="font-italic">Tender chicken, onion, capsicum and stretchy mozzarella.</font>
                            </Col>
                            <Col md="2 text-uppercase">
                                <p className="h5-responsive text-uppercase font-weight text-warning">5<font size="2">95</font></p>
                            </Col>
                        </Row>                
                    </Col>
                </Row>
            </View>   
        </View>

    );
  }
}

export default Section6;