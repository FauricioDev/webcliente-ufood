import React, { Component } from 'react';
import  {Carousel, CarouselCaption, CarouselInner, CarouselItem, View, Mask, Container, Row, Col, CardGroup, Card, CardText, CardBody, CardImage, CardTitle, Button, Footer, Table, Input, Fa } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import './home.css'
import slide2 from '../../img/02.jpg'





class Section4 extends Component {
    
      
  render(){
    return(
     
    <View className="container pt-5 d-flex justify-content-center">
        <section className="text-center">
                
            <Tabs contentClass="tab-content" headerClass="tab-header-bold" onSelect={(index, label) => console.log(`Selected Index: ${index}, Label: ${label}`)}>
                <Tab label="Wraps" className="Wrap-content">
                <Row className="p-3">
                            <div className="col-sm-12 col-md-3">
                                    <View className="home">
                                        <div className="container-fluid text-center">
                                            <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                                                <p className="h3-responsive font-weight-bold">Wraps</p>
                                        </div>
                                        <div className="container-fluid pb-3">
                                            
                                                
                                                    <View className="container-fluid">
                                                        <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                        <Row className="d-flex justify-content-between">
                                                            <Col><hr/></Col>
                                                            <Col><p><font size="1">Pick Size</font></p></Col>
                                                            <Col><hr/></Col>
                                                        </Row>
                                                        <Row className="px-1 d-flex justify-content-center">
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                                </span>                                                        
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <p className="px-1 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                                </span>
                                                            </Col-3>
                                                            <Col-3>
                                                                <p className="px-1 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                        </Row>
                                                    </View>
                                                
                                                <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                           
                                        </div>
                                    </View>
                                </div>
                                <div className="col-sm-12 col-md-3">
                                    <View className="home">
                                        <div className="container-fluid text-center">
                                            <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                                                <p className="h3-responsive font-weight-bold">Wraps 2</p>
                                        </div>
                                        <div className="container-fluid pb-3">
                                            
                                                
                                                    <View className="container">
                                                        <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                        <Row className="d-flex justify-content-between">
                                                            <Col><hr/></Col>
                                                            <Col><p><font size="1">Pick Size</font></p></Col>
                                                            <Col><hr/></Col>
                                                        </Row>
                                                        <Row className="px-1 d-flex justify-content-center">
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                                </span>                                                        
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <p className="px-1 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                                </span>
                                                            </Col-3>
                                                            <Col-3>
                                                                <p className="px-1 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                        </Row>
                                                    </View>
                                                
                                                <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                           
                                        </div>
                                    </View>
                                </div>
                                <div className="col-sm-12 col-md-3">
                                    <View className="home">
                                        <div className="container-fluid text-center">
                                            <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                                                <p className="h3-responsive font-weight-bold">Wraps 3</p>
                                        </div>
                                        <div className="container-fluid pb-3">
                                            
                                                
                                                    <View className="container">
                                                        <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                        <Row className="d-flex justify-content-between">
                                                            <Col><hr/></Col>
                                                            <Col><p><font size="1">Pick Size</font></p></Col>
                                                            <Col><hr/></Col>
                                                        </Row>
                                                        <Row className="px-1 d-flex justify-content-center">
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                                </span>                                                        
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <p className="px-1 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                                </span>
                                                            </Col-3>
                                                            <Col-3>
                                                                <p className="px-1 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                        </Row>
                                                    </View>
                                                
                                                <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                           
                                        </div>
                                    </View>
                                </div>
                                <div className="col-sm-12 col-md-3">
                                    <View className="home">
                                        <div className="container-fluid text-center">
                                            <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                                                <p className="h3-responsive font-weight-bold">Wraps 2</p>
                                        </div>
                                        <div className="container-fluid pb-3">
                                            
                                                
                                                    <View className="container">
                                                        <p className="text-center" style={{color: 'rgba(189, 189, 189, 0.958)'}}><em>Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped with lightly drizzled red souce, pesto, and fresh basil</em></p>
                                                        <Row className="d-flex justify-content-between">
                                                            <Col><hr/></Col>
                                                            <Col><p><font size="1">Pick Size</font></p></Col>
                                                            <Col><hr/></Col>
                                                        </Row>
                                                        <Row className="px-1 d-flex justify-content-center">
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                                                </span>                                                        
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <p className="px-1 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                            <Col-3 className="">
                                                                <span className="fa-stack fa-lg">
                                                                    <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                                    <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                                                </span>
                                                            </Col-3>
                                                            <Col-3>
                                                                <p className="px-1 pt-2">
                                                                    <font size="1" sytle={{color:'black'}} className="">$</font>
                                                                19.90
                                                                </p>
                                                            </Col-3>
                                                        </Row>
                                                    </View>
                                                
                                                <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                                           
                                        </div>
                                    </View>
                                </div>
                            </Row>
                    </Tab> 
                </Tabs>
            </section>
        </View>


    );
  }
}

export default Section4;