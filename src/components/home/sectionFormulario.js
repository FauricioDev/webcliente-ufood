import React, { Component } from 'react';
import  {Carousel, CarouselCaption, CarouselInner, CarouselItem, View, Mask, Container, Row, Col, CardGroup, Card, CardText, CardBody, CardImage, CardTitle, Button, Footer, Table, Input, Fa } from 'mdbreact';
import './home.css'
import googlePlay from '../../img/google-play.png'
import AppStore from '../../img/app-store.png'






class SectionFormulario extends Component {
    
      
  render(){
    return(
        
        <View className="pt-3 bgback" >
            <Row className="d-flex justify-content-center">
                <Col md="10 text-center text-uppercase">
                    <p className="h2 strong">Descarga nuestra app cliente para</p>
                    <p className="h2 strong">Android y Iphone</p>
                </Col>
            </Row>
            <Row className="d-flex justify-content-center">
                <Col md="3">
                    <img className="img-fluid" src={googlePlay} alt="google-play" />
                </Col>
                <Col md="3">
                    <img className="img-fluid" src={AppStore} alt="App-Store" />
                </Col>
            </Row>
            <Row className="d-flex justify-content-center">
            <Col md="6">
                <Card style={{backgroundColor: ''}}>
                <CardBody>
                    <form>
                        <p className="h3 text-center mb-4">Escribele a UFood</p>
                        <div className="grey-text">
                            <Input label="Your name"  icon="user" group type="text" validate error="wrong" success="right"/>
                            <Input label="Your email" icon="envelope" group type="email" validate error="wrong" success="right"/>
                            <Input type="textarea" rows="2" label="Your message to us..." icon="pencil"/>
                        </div>
                        <div className="text-center">
                            <Button outline className="badge-pill danger" color="danger">Send Message<Fa icon="paper-plane-o" className="ml-1"/></Button>
                        </div>
                    </form>
                </CardBody>
                </Card>
            </Col>
            </Row>
            <Row className="d-flex justify-content-center">
                <Col md="3">
                    <img className="img-fluid" src={googlePlay} alt="google-play" />
                </Col>
                <Col md="3">
                    <img className="img-fluid" src={AppStore} alt="App-Store" />
                </Col>
            </Row>
            <Row className="d-flex justify-content-center">
                <Col md="10 text-center text-uppercase">
                    <p className="h2 strong">Descarga nuestra app cliente para</p>
                    <p className="h2 strong">Android y Iphone</p>
                </Col>
            </Row>
        </View>
    );
  }
}

export default SectionFormulario;