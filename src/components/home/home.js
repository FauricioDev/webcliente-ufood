import React, { Component } from 'react';
import  {Carousel, CarouselCaption, CarouselInner, CarouselItem, View, Mask, Container, Row, Col, CardGroup, Card, CardText, CardBody, CardImage, CardTitle, Button, Footer, Table, Input, Fa } from 'mdbreact';
import './home.css'
import Foot from '../foot/foot';
import Section1 from './section1';
import Section2 from './section2';
import Section3 from './section3';
import Section4 from './section4';
import Section5 from './section5';
import Section6 from './section6';
import SectionFormulario from './sectionFormulario';

class Home extends Component {    
      
  render(){
    return(
        <div>
            {/* <Menu/> */}
            <Section1/>
            <Section2/>
            <Section3/>
            <View className="fluid-container">
                <View className="bgbanner" >
                <Mask overlay="black-strong"></Mask>
                    <Row className="d-flex justify-content-center">
                        <Col md="10 text-center text-uppercase py-5">
                            <p className="h6-responsive text-uppercase font-weight-normal">______Free delivery with_____</p>
                            <p className="h1-responsive font-weight-bold text-uppercase">Pizza of the day</p>
                            <p className="h1 font-weight-bold">9<font size="4">99</font></p>
                            <Button color="danger" size="md" className="rounded">Order Now</Button>
                        </Col>
                    </Row>
                </View>   
            </View>
            <Section4/>
            <Section5/>
            <Section6/>
            <SectionFormulario/>
            <Foot/>

        </div> //container padre
    );
  }
}

export default Home;