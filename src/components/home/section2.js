import React, { Component } from 'react';
import  {Carousel, CarouselCaption, CarouselInner, CarouselItem, View, Mask, Container, Row, Col, CardGroup, Card, CardText, CardBody, CardImage, CardTitle, Button, Footer, Table, Input, Fa } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import './home.css'
import g1 from '../../img/04.jpg'
import g2 from '../../img/05.jpg'
import g3 from '../../img/08.jpg'
import g4 from '../../img/07.jpg'

class Section2 extends Component {
    
      
  render(){
    return(
       
      <div className="container py-5">
        <Row className="py-5">
            <Col>
                <img src={g1} className="img-fluid" alt="g1"/>
            </Col>
            <Col>
                <Row>
                    <Col>
                        <img src={g2} className="img-fluid" alt="g2"/>
                    </Col>
                    <Col>
                        <img src={g4} className="img-fluid" alt="g4" />
                    </Col>
                </Row>
                <Row className="pt-2">
                    <Col>
                        <img src={g3} className="img-fluid" alt="g3"/>
                    </Col>
                </Row>
            </Col>
        </Row>
      </div>
      

    );
  }
}

export default Section2;