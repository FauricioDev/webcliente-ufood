// import './restaurante.css'
import React, { Component } from 'react';
import { Button, Collapse, View, Row, Col, Fa, PageItem, PageLink, Pagination } from 'mdbreact';
import burger from '../../img/01.jpg'
import './perfil-restaurante.css';
import url from "../../config/url";
// importacion de componentes reutilizables
 import CardViewProduct from './components/cardViewProduct';
import axios from 'axios';

class Section1 extends Component {
    constructor(props){
        super(props);
        this.state={
            allProduct: null,
            token:'',
            idComercio: '',
            stateType: ''
        }
    }
 componentWillMount =()=>{
     console.log("=======================");
     console.log("stateType de la section 1 en PERFIL-RESTAURANTE", this.props.stateType);
     console.log("=======================");
    this.getAllProducts();
 }

 getAllProducts =async()=>{
     const { idRestaurant, stateType } = this.props;
    const token = await localStorage.getItem('userToken');
    this.setState({token, idComercio: idRestaurant, stateType})
    console.log('el id del restaurante', idRestaurant, 'token=>', token);

    axios.get(`http://appufood.com:5000/api/v1/gst-comercios/productos/list-all-commerce-productos/${this.props.idRestaurant}/0`, {
        headers:{
            'Content-Type': 'application/json',
            Authorization: token
        }
    })
    .then((allProduct)=>{
        console.log('respuesta al traer todos los productos', allProduct['data'], allProduct)
        this.setState({allProduct: allProduct['data']})
    })
    .catch(e => console.error('error al traer todos los productos',e))
 }

  render() {
    return (
    
        <div>
        <hr/>
        <p className="h1-responsive text-center font-weight-bold text-uppercase">{this.props.nombre}</p>
        <br/>
            <div className="row" > 
            {/* agregar el flex-wrap al div de arriba */}
                {(this.state.allProduct !== null) ?
                    this.state.allProduct.map((item,index, array)=>{
                        console.log('soy el item que recibe', item);
                        const { nombre_producto, precio, descripcion, _id}= item;
                        return (
                            <CardViewProduct
                                key={index}
                                nombre={nombre_producto}
                                precio={precio}
                                descripcion={descripcion}
                                seeProduct={()=>localStorage.setItem('idProduct',_id)}
                                idProduct ={_id}
                                idComercio={this.state.idComercio}
                                token={this.state.token}
                                stateType={this.props.stateType}
                                nombre_categoria={item.categoria}

                            />
                        )
                    })
                    : 
                    <h1>Este restaurante no tiene productos todavia</h1>
                }
            </div>
            <hr className="mt-5"/>
      </div>
    );
  }
}
export default Section1;