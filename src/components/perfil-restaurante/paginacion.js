// import './restaurante.css'
import React, { Component } from 'react';
import { Button, Collapse, View, Row, Col, Fa, PageItem, PageLink, Pagination } from 'mdbreact';
import slide2 from '../../img/02.jpg'
import { Container } from 'reactstrap';
import Foot from '../foot/foot';
import Imagenes from '../imagenes/imagenes';



class Paginacion extends Component {
 

  render() {
    return (
        <div className="container">
            <nav aria-label="pagination example">
                <ul class="pagination pagination-circle pg-red mb-0 justify-content-center py-4">
                    <li class="page-item disabled">
                        <a class="page-link" href="#">Previous</a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="perfil-restaurante">1</a></li>
                    <li class="page-item"><a class="page-link" href="perfil-restaurante-2">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" to="#">4</a></li>
                    <li class="page-item"><a class="page-link" to="#">5</a></li>
                    <li class="page-item">
                        <a class="page-link" to="#">Next</a>
                    </li>
                </ul>
            </nav>
        </div>  
    );
  }
}
export default Paginacion;