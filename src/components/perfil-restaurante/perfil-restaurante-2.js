// import './restaurante.css'
import React, { Component } from 'react';
import { Button, Collapse, View, Row, Col, Fa, PageItem, PageLink, Pagination } from 'mdbreact';
import slide2 from '../../img/02.jpg'
import { Container } from 'reactstrap';
import Foot from '../foot/foot';
import Imagenes from '../imagenes/imagenes';
import Paginacion from './paginacion';




class PerfilRestaurante2 extends Component {
 

  render() {
    return (
    <div style={{marginTop:150}}>
        <div className="container py-5 px-5">
        <hr/>
        <p className="h1-responsive text-center font-weight-bold text-uppercase">Mc donalds2</p>
        <br/>
            <div className="row">
                <div className="col-md-6 col-xs-12">
                    <div className="row text-center">
                        <div className="col-4">
                            <img src={slide2} alt="01" height="150"  className="img-fluid" />
                        </div>
                        <div className="col-8">
                            <p className="h5-responsive text-uppercase"><strong>Big mac</strong></p>
                            <p className="font-italic">Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped</p>
                            <Row className="d-flex justify-content-center">
                                <Col><hr/></Col>
                                <Col><p>Precio</p></Col>
                                <Col><hr/></Col>
                            </Row>
                            <p>$ XX.XXX</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 col-xs-12">
                    <div className="row text-center">
                        <div className="col-4">
                            <img src={slide2} alt="01" height="150"  className="img-fluid" />
                        </div>
                        <div className="col-8">
                            <p className="h5-responsive text-uppercase"><strong>Big mac</strong></p>
                            <p className="font-italic">Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped</p>
                            <Row className="d-flex justify-content-center">
                                <Col><hr/></Col>
                                <Col><p>Precio</p></Col>
                                <Col><hr/></Col>
                            </Row>
                            <p>$ XX.XXX</p>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-md-6 col-xs-12">
                    <div className="row text-center">
                        <div className="col-4">
                            <img src={slide2} alt="01" height="150"  className="img-fluid" />
                        </div>
                        <div className="col-8">
                            <p className="h5-responsive text-uppercase"><strong>Big mac</strong></p>
                            <p className="font-italic">Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped</p>
                            <Row className="d-flex justify-content-center">
                                <Col><hr/></Col>
                                <Col><p>Precio</p></Col>
                                <Col><hr/></Col>
                            </Row>
                            <p>$ XX.XXX</p>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 col-xs-12">
                    <div className="row text-center">
                        <div className="col-4">
                            <img src={slide2} alt="01" height="150"  className="img-fluid" />
                        </div>
                        <div className="col-8">
                            <p className="h5-responsive text-uppercase"><strong>Big mac</strong></p>
                            <p className="font-italic">Ricotta, sun dried tomatoes, garlic, mozzarella cheese, topped</p>
                            <Row className="d-flex justify-content-center">
                                <Col><hr/></Col>
                                <Col><p>Precio</p></Col>
                                <Col><hr/></Col>
                            </Row>
                            <p>$ XX.XXX</p>
                        </div>
                    </div>
                </div>
            </div>

            <hr className="mt-5"/>

            <Paginacion/>

        </div>  {/* fin-div-container  */}
            <Imagenes/>
            <Foot/>
    </div>

    );
  }
}
export default PerfilRestaurante2;