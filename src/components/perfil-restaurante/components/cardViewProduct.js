import React, { Component } from 'react';
import { Button, Collapse, View, Row, Col, Fa, PageItem, PageLink, Pagination } from 'mdbreact';
import burger from '../../../img/01.jpg';
import '../perfil-restaurante.css';
import { BrowserRouter, Redirect , Route, Switch, Link } from "react-router-dom";

export const CardViewProduct =(props)=>{
    console.log(props)
    return (
            <Link  to={{ pathname: "/orden-pedido" ,params: { idComercio:props.idComercio, idProduct: props.idProduct, token: props.token, stateType: props.stateType, nombre_categoria: props.nombre_categoria }}}>
                <div className="cardViewProduct col-md-6 col-xs-12" onClick={props.seeProduct}>
                    <div className="row">
                        <div className="col-4 d-flex align-items-center">
                            <img src={burger} alt="01" height="150" className="img-fluid" />
                        </div>
                        <div className="col-8 text-center">
                            <p className="h5-responsive text-uppercase"><strong>{props.nombre}</strong></p>
                            <p className="font-italic">{props.descripcion}</p>
                            <Row className="d-flex justify-content-center">
                                <Col><hr/></Col>
                                <Col><p>Precio</p></Col>
                                <Col><hr/></Col>
                            </Row>
                            <p>$ {props.precio}</p>
                        </div>
                    </div>
                </div>
            </Link>
    );
}


export default CardViewProduct;
