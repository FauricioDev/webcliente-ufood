// import './restaurante.css'
import React, { Component } from "react";
import {
  Button,
  Collapse,
  View,
  Row,
  Col,
  Fa,
  PageItem,
  PageLink,
  Pagination
} from "mdbreact";
import burger from "../../img/01.jpg";
import { Container } from "reactstrap";
import Foot from "../foot/foot";
import Imagenes from "../imagenes/imagenes";
import Paginacion from "./paginacion";
import Section1 from "./section1";

class PerfilRestaurante extends Component {
  state = {
    nombre: null,
    idRestaurant: null,
    stateType: "PerfilRestaurante"
  };

  componentWillMount = () => {
            const { nombre, idRestaurant, stateType } = this.props.location.params;
        console.log('lo que se recibe por la ruta',this.props.location.params);
        console.log(nombre, idRestaurant);
        console.log("=================================");
        console.log("stateType : =>", stateType);
        console.log("=================================");
        this.setState({ nombre, idRestaurant, stateType});
  };

  render() {
    return (
      <div style={{ marginTop: 150 }}>
        <div className="container py-5 px-5">
          <Section1 nombre={this.state.nombre} idRestaurant={this.state.idRestaurant} stateType={this.state.stateType}/>,
          <Paginacion />
        </div>{" "}
        {/* fin-div-container  */}
        <Imagenes />
        <Foot />
      </div>
    );
  }
}
export default PerfilRestaurante;
