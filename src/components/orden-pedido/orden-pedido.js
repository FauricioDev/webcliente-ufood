import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Button, Collapse, View, Row, Col, Fa, PageItem, PageLink, Pagination, Input } from 'mdbreact';
import burger from '../../img/01.jpg'
import { Container } from 'reactstrap';
import Foot from '../foot/foot';
import Imagenes from '../imagenes/imagenes';
import './orden-pedido.css';
import slide2 from '../../img/02.jpg'
import Section1 from './section1';
import Section2 from './section2';
import Section3 from './section3';
import url from '../../config/url';


// const {foo} = this.props.location.state

class OrdenPedido extends PureComponent {
  state={
    allProduct: 'hola',
    allModificadores: null,
    token: null,
    idComercio: null,
    idProduct: null

 }
 componentWillMount=async()=>{
    const stateTypecito = localStorage.getItem('stateType');
    console.log("================================");
    console.log("================================");
    console.log("================================");
    console.log("================================");
    console.log("stateType: ", stateTypecito);
    console.log("================================");
    console.log("================================");
    console.log("================================");
   console.log('PROPS', this.props);
   const { token, idProduct, idComercio, stateType, nombre_categoria} = this.props.location.params;
   this.setState({
     token,
     idComercio,
     idProduct,
     stateType: stateTypecito,
     nombre_categoria
   })

   
 }

  render() {
    return (
    <div style={{marginTop:150}}>
            <Section1 idProduct={this.state.idProduct} token={this.state.token}/>
            <Section2 idComercio ={this.state.idComercio} idProduct={this.state.idProduct} token={this.state.token} stateType={this.state.stateType} nombre_categoria={this.state.nombre_categoria}  />
            {/* <Section3/> */}
            <Foot/>
    </div>

    );
  }
}

// OrdenPedido.prototype = {
//   token: PropTypes.string.isRequired,
//   idProduct: PropTypes.string.isRequired
// }
// OrdenPedido.defaultProps = {
//   token: '',
//   idProduct: '',
// }
export default OrdenPedido;