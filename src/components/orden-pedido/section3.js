import React, { Component } from 'react';
import { Button, Collapse, View, Row, Col, Fa, PageItem, PageLink, Pagination, Input } from 'mdbreact';
import burger from '../../img/01.jpg'
import './orden-pedido.css';
import slide2 from '../../img/02.jpg'


class Section3 extends Component {
   
   
  render() {
    return (
        <div className="container pt-4 text-center">
        <p className="h4-responsive"><strong>Acompaña tu pedido con</strong></p>
        <hr className="pb-3" />
        <Row className="p-3">
        <div className="col-sm-12 col-md-3">
                <View className="pedido">
                    <div className="container-fluid text-center">
                        <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                            <p className="h4-responsive">Cola Light</p>
                    </div>
                    <div className="container-fluid pb-3">

                                <View className="container-fluid">
                    
                                    <Row className="d-flex justify-content-between">
                                        <Col><hr/></Col>
                                        <Col><p><font size="1">Pick Size</font></p></Col>
                                        <Col><hr/></Col>
                                    </Row>
                                    <Row className="px-1 d-flex justify-content-center">
                                        <Col-3 className="">
                                            <span className="fa-stack fa-lg">
                                                <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                            </span>                                                        
                                        </Col-3>
                                        <Col-3 className="">
                                            <p className="px-1 pt-2">
                                                <font size="1" sytle={{color:'black'}} className="">$</font>
                                            19.90
                                            </p>
                                        </Col-3>
                                        <Col-3 className="">
                                            <span className="fa-stack fa-lg">
                                                <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                            </span>
                                        </Col-3>
                                        <Col-3>
                                            <p className="px-1 pt-2">
                                                <font size="1" sytle={{color:'black'}} className="">$</font>
                                            19.90
                                            </p>
                                        </Col-3>
                                    </Row>
                                </View>
                            
                            <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                        
                    </div>
                </View>
            </div>
            <div className="col-sm-12 col-md-3">
                <View className="pedido">
                    <div className="container-fluid text-center">
                        <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                            <p className="h4-responsive">Fries</p>
                    </div>
                    <div className="container-fluid pb-3">

                                <View className="container">
                    
                                    <Row className="d-flex justify-content-between">
                                        <Col><hr/></Col>
                                        <Col><p><font size="1">Pick Size</font></p></Col>
                                        <Col><hr/></Col>
                                    </Row>
                                    <Row className="px-1 d-flex justify-content-center">
                                        <Col-3 className="">
                                            <span className="fa-stack fa-lg">
                                                <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                            </span>                                                        
                                        </Col-3>
                                        <Col-3 className="">
                                            <p className="px-1 pt-2">
                                                <font size="1" sytle={{color:'black'}} className="">$</font>
                                            19.90
                                            </p>
                                        </Col-3>
                                        <Col-3 className="">
                                            <span className="fa-stack fa-lg">
                                                <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                            </span>
                                        </Col-3>
                                        <Col-3>
                                            <p className="px-1 pt-2">
                                                <font size="1" sytle={{color:'black'}} className="">$</font>
                                            19.90
                                            </p>
                                        </Col-3>
                                    </Row>
                                </View>
                            
                            <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                        
                    </div>
                </View>
            </div>
            <div className="col-sm-12 col-md-3">
                <View className="pedido">
                    <div className="container-fluid text-center">
                        <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                            <p className="h4-responsive">Chocolate Cookie</p>
                    </div>
                    <div className="container-fluid pb-3">

                                <View className="container">
                    
                                    <Row className="d-flex justify-content-between">
                                        <Col><hr/></Col>
                                        <Col><p><font size="1">Pick Size</font></p></Col>
                                        <Col><hr/></Col>
                                    </Row>
                                    <Row className="px-1 d-flex justify-content-center">
                                        <Col-3 className="">
                                            <span className="fa-stack fa-lg">
                                                <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                            </span>                                                        
                                        </Col-3>
                                        <Col-3 className="">
                                            <p className="px-1 pt-2">
                                                <font size="1" sytle={{color:'black'}} className="">$</font>
                                            19.90
                                            </p>
                                        </Col-3>
                                        <Col-3 className="">
                                            <span className="fa-stack fa-lg">
                                                <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                            </span>
                                        </Col-3>
                                        <Col-3>
                                            <p className="px-1 pt-2">
                                                <font size="1" sytle={{color:'black'}} className="">$</font>
                                            19.90
                                            </p>
                                        </Col-3>
                                    </Row>
                                </View>
                            
                            <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                        
                    </div>
                </View>
            </div>
            <div className="col-sm-12 col-md-3">
                <View className="pedido">
                    <div className="container-fluid text-center">
                        <img src={slide2} alt="01" height="200" className="rounder img-fluid" style={{ marginBottom: "1.5rem", marginTop:"1rem"   }} />
                            <p className="h4-responsive">Rasbery Ice</p>
                    </div>
                    <div className="container-fluid pb-3">

                                <View className="container">
                    
                                    <Row className="d-flex justify-content-between">
                                        <Col><hr/></Col>
                                        <Col><p><font size="1">Pick Size</font></p></Col>
                                        <Col><hr/></Col>
                                    </Row>
                                    <Row className="px-1 d-flex justify-content-center">
                                        <Col-3 className="">
                                            <span className="fa-stack fa-lg">
                                                <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                <p><font size="1" sytle={{color:'black'}}>22</font></p>

                                            </span>                                                        
                                        </Col-3>
                                        <Col-3 className="">
                                            <p className="px-1 pt-2">
                                                <font size="1" sytle={{color:'black'}} className="">$</font>
                                            19.90
                                            </p>
                                        </Col-3>
                                        <Col-3 className="">
                                            <span className="fa-stack fa-lg">
                                                <i className="fa fa-circle-thin fa-stack-2x" style={{color:'green'}}></i>
                                                <p><font size="1" sytle={{color:'black'}}>22</font></p>
                                            </span>
                                        </Col-3>
                                        <Col-3>
                                            <p className="px-1 pt-2">
                                                <font size="1" sytle={{color:'black'}} className="">$</font>
                                            19.90
                                            </p>
                                        </Col-3>
                                    </Row>
                                </View>
                            
                            <Button color="danger" className="badge-pill" size="md"><Fa icon="clipboard" className="mr-1"/>Agregar</Button>
                        
                    </div>
                </View>
            </div>
        </Row>
    </div>

    );
  }
}
export default Section3;