import React, { Component } from 'react';
import { Button, Collapse, View, Row, Col, Fa, PageItem, PageLink, Pagination, Input } from 'mdbreact';
import PropTypes from 'prop-types';
import burger from '../../img/01.jpg'
import './orden-pedido.css';
import url from '../../config/url';



class Section1 extends Component {
  state={
      allProduct: null
  }
componentDidMount(){
    console.log('sesion marica 1', this.props)
    this.getProductById();
}   
getProductById =async()=>{
    const {idProduct, token} = this.props;
    fetch(`${url}gst-comercios/productos/list-single-product/${idProduct}/`,{
      headers:{
        'Content-Type': 'app/json',
        Authorization: token
      }
    })
    .then(res=>res.json())
    .then(allProduct=> {
        console.log('la respuesta al traer el product', allProduct);
        this.setState({allProduct}, ()=> console.log('despues de traer todo el producto', this.state.allProduct));
  })
    .catch(e =>console.error('error al traer el producto', e));
  }
  render() {
    return (
    <div>
    <div className="container pt-5">
        <ul className="breadcrumb bg-pagnav px-5" style={{backgroundColor:'white'}}>
            <li className="breadcrumb-item"><a href="/home" style={{color:'black'}}>Home</a></li>
            <li className="breadcrumb-item"><a href="/restaurante" style={{color:'black'}}>Restaurante</a></li>
            <li className="breadcrumb-item"><a href="/perfil-restaurante" style={{color:'black'}}>nombre restaurante</a></li>
            <li className="breadcrumb-item active">{(this.state.allProduct !== null)? this.state.allProduct.nombre_producto: 'cargando'}</li>
        </ul>
    </div>
        <hr/>
        <div className="container px-5">
        {/* <p className="h1-responsive text-center font-weight-bold text-uppercase"></p> */}
        <br/>
            <div className="row">
                <div className="col-4">
                    <img src={burger} alt="01" height="150" className="img-fluid" />
                </div>
                <div className="col-1">
                    <img src={burger} alt="01" className="img-fluid pb-1" />
                    <img src={burger} alt="01" className="img-fluid pt-1" />
                </div>
                <div className="col-7">
                    <p className="h5-responsive text-uppercase"><strong>{(this.state.allProduct !== null)? this.state.allProduct.nombre_producto: 'cargando'}</strong></p>
                    <p className="h5-responsive text-uppercase"><strong>{(this.state.allProduct !== null)? this.state.allProduct.precio: 'cargando'} $</strong></p>

                    <hr/>
                    <button type="button" social="fb" className="btn btn-indigo btn-sm"><Fa icon="facebook" className="pr-1"/> Like</button>
                    <button type="button" social="tw" className="btn btn-blue btn-sm"><Fa icon="twitter" className="pr-1"/> twitter</button>
                    <button type="button" social="gplus" className="btn btn-white btn-sm"><Fa icon="google" className="pr-1"/> +</button>
                    <p className="grey-text pt-4">{(this.state.allProduct !== null)? this.state.allProduct.descripcion: 'cargando'}</p>
                </div>
            </div>
        </div>
    </div>

    );
  }
}

// Section1.prototype = {
//     token: PropTypes.string.isRequired,
//     idProduct: PropTypes.string.isRequired
// }
// Section1.defaultProps = {
//     token: '',
//     idProduct: '',
// }
export default Section1;