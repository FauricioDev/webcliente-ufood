import React, { Component } from "react";
import {
  Button,
  Collapse,
  View,
  Row,
  Col,
  Fa,
  PageItem,
  PageLink,
  Pagination,
  Input
} from "mdbreact";
import { BrowserRouter, Redirect, Route, Switch, Link } from "react-router-dom";

import burger from "../../img/01.jpg";
import "./orden-pedido.css";
import axios from "axios";
import url from "../../config/url";
import { connect } from "react-redux";

import { ordenCreate } from "../../store/actions/orden";
var precioTotal = 0;
class Section2 extends Component {
  constructor(props){
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.state = {
      allModificators: null,
      precio: 0,
      precioProducto: 0,
      final: null,
      ordenCompleta: null,
      orden: null,
      producto: [],
      idRestaurant: null,
      valorFinal: 0, 
      cantidad: 1,
      stateType: 0
    };
  }
  componentDidMount=async()=> {
    console.log("=============================");
    console.log("stateType de confirmar la orden: ", this.props.stateType);
    console.log("=============================");

    this.getProductById();
    let ordenCompleta = await localStorage.getItem("ordenes");
   
    console.log('esta es la roden completa de la secction2',JSON.parse(ordenCompleta));
    if (JSON.parse(ordenCompleta)) {
      ordenCompleta = JSON.parse(localStorage.getItem("ordenes"));
      console.log("local storiage =>>>>>>>>>>>>>>><", ordenCompleta);
    } else {
      // ordenCompleta = this.props.orden;
    }
    console.log("ORDEEEENNNNNNN REDUXXX ====================>", ordenCompleta);
    this.setState({ ordenCompleta });
  }
  getProductById = async () => {
    const { idProduct, token } = this.props;
    fetch(`${url}gst-comercios/productos/list-single-product/${idProduct}/`, {
      headers: {
        "Content-Type": "app/json",
        Authorization: token
      }
    })
      .then(res => res.json())
      .then(allModificators => {
        const final = allModificators.adiciones.map(r => {
          const temporal = r.adiciones.map(i => {
            return { ...i, check: false };
          });
          return { ...r, adiciones: temporal };
        });
        const final1 = { ...allModificators, adiciones: final };
        this.setState(
          {
            allModificators: final1.adiciones,
            precioProducto: final1.precio,
            producto: allModificators,
            precio: final1.precio
          },
        );
      })
      .catch(e => console.error("error al traer el producto", e));
  };
  addProducto = (llave, item) => {
    const temp = this.state.allModificators[llave].adiciones.map(
      (val, index) => {
        if (index === item) {
          val.check = true;
        } else {
          val.check = false;
        }
        return val;
      }
    );
    const { allModificators } = this.state;
    allModificators[llave].adiciones = temp;
    this.setState({ allModificators });
  };
  addPrice = () => {
    const {
      allModificators,
      precioProducto,
      producto,
    } = this.state;
    let val = precioProducto;
    const array = allModificators;
    const adicion = [];
    const select = [];
    for (let i = 0; i < array.length; i++) {
      const t = array[i].adiciones;
      for (let f = 0; f < t.length; f++) {
        adicion.push({
          precio: t[f].precio,
          check: t[f].check,
          _id: t[f]._id,
          categoria: t[f].categoria,
          nombre_adicion: t[f].nombre_adicion
        });
      }
    }
    adicion.map(item => {
      if (item.check) {
        select.push(item);
        val += item.precio;
      }
    });
    this.setState({ valorFinal: val });
    producto["adiciones"] = select;
    producto["total"] = val*this.state.cantidad;
    producto["cantidad"]= this.state.cantidad;
    producto["nombre_categoria"]= this.props.nombre_categoria;
  };
  addOrden = async() => {

    this.addPrice();
    const { idComercio } = this.props;
    const { ordenCompleta, valorFinal, cantidad, producto } = this.state;
    console.log('lo que esta recibiendo desde el estado orden completa: >>>', ordenCompleta);
    const temp = ordenCompleta["productos"].filter(
      res => res.comercio === idComercio
    );
    const temp2 = ordenCompleta["productos"].filter(
      res => res.comercio !== idComercio
    );
    if (temp.length > 0) {
      temp[0].productos.push(producto);
      temp2.push({...temp[0]});
      ordenCompleta.productos = temp2;
    } else {
      const body = {
        stateType:this.props.stateType,
        comercio: idComercio,
        productos: [producto],
        observaciones: "observaciones",
        ultima_modificacion: new Date(),
        nombre_categoria:this.props.nombre_categoria,
        total: 0
      };
      ordenCompleta.productos.push(body);
    }
    // localStorage.removeItem("ordenes");
    localStorage.setItem("ordenes", JSON.stringify(ordenCompleta));
    const OrdenFull = await localStorage.getItem("ordenes");
    console.log("==========================Orden completa=================");
    console.log(OrdenFull);
    console.log("==========================Orden completa=================");
  };
  handleInputChange=(event)=> {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }
  render() {
    // console.log("orden completa", this.state.orden);
    return (
      <div className="my-5 bg-sabores">
        <div className="container py-5 px-5">
          <p className="h6-responsive pt-5">
            <strong>Adiciones</strong>
          </p>
          <hr />
          <form className="row pt-3">
            {Array.isArray(this.state.allModificators) &&
              this.state.allModificators.map((item, i) => {
                return (
                  <div className="col-4">
                    <p>
                      <strong>{item.nombre_categoria}</strong>
                    </p>

                    {Array.isArray(item.adiciones) &&
                      item.adiciones.map((item1, index) => {
                        return (
                          <div
                            onClick={() => {
                              console.log(item1);
                              this.addProducto(i, index);
                              this.addPrice();
                            }}
                          >
                            <input
                              type="radio"
                              id={index}
                              name={item.nombre_categoria}
                              value={item1.nombre_adicion}
                            />
                            <label for={item1.nombre_adicion}>
                              {item1.nombre_adicion}{" "}
                              <span className="grey-text" size="2">
                                {item1.precio}
                              </span>
                            </label>
                          </div>
                        );
                      })}
                  </div>
                );
              })}
          </form>

          <p className="h6-responsive pt-5">
            <strong>Order Summary</strong>
          </p>
          <hr />
          <div className="row">
            <div className="col-5">
              <label for="order">
                <strong>Order Special Wish</strong>
              </label>
              <input
                type="text"
                id="order"
                name="order"
                className="form-control py-2"
                style={{ borderRadius: "50px" }}
                placeholder="Type here your special order wish..."
              />
            </div>
            <div className="col-2">
              <label for="cantidad">
                <strong>Quanity</strong>
              </label>
              <input
                type="number"
                id="cantidad"
                name="cantidad"
                className="form-control py-2"
                style={{ borderRadius: "50px" }}
                placeholder="01"
                value={this.state.cantidad}
                onChange={this.handleInputChange}
              />
            </div>
            <div className="col-2 align-self-end">
              <br />
              <p>
                <font size="1">$ </font>
                {this.state.precio}
              </p>
            </div>
            <div className="col-3 align-self-end">
              <Link to={{
                pathname:"/perfil-restaurante",
                params:{nombre:'', idRestaurant: this.props.idComercio }
                }
              }
                >
                <Button
                  onClick={() => this.addOrden()}
                  className="badge-pill"
                  color="danger"
                  size="md"
                >
                  <Fa icon="clipboard" className="mr-1" />
                  Agregar
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    orden: state.orden.orden
  };
};
const mapDispatchToProps = {
  ordenCreate
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Section2);
