import React, { Component } from 'react';
import  {Stepper, Step, Container, Row, Col, Button, Modal, ModalBody, ModalFooter, ModalHeader, View, Mask } from 'mdbreact';
import './calificacion.css';
// import chulo from '../../img/icons/check.png'
// import chulo from '../../img/icons/chulo.png'
import reloj from '../../img/icons/reloj.png'
import Foot from '../foot/foot';
import Imagenes from '../imagenes/imagenes';
import urunner from '../../img/icons/urunner.png'
import logo from '../../img/logo-blanco.png'
import modal from '../../img/mac-modal.png'
import face from '../../img/face.png'
import Section1 from './section1';
import Navegation from './navegation';
import Section2 from './section2';



class Calificacion extends Component {
    constructor(props) {
        super(props);
        this.state = {
          modal: false
        };
    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }
   
  
    render() {
      return(

        <div style={{marginTop:150}}>
                <Navegation/>
                 <hr/>
            <div className="container px-5 py-4">
                {/* <Section1/> */}
                <br/><br/>
                <hr />
                <Section2/>
                 <View>
                     <Row>
                         <Col md="12" className="d-flex justify-content-end">
                            <img src={urunner} height="100" onClick={this.toggle} className />
                         </Col>
                     </Row>
                 </View>
                
                <Modal isOpen={this.state.modal} toggle={this.toggle} side position="top-right">
                <View className="bgmodal img-fluid ">
                <div class="bgheader flex-center">
                <ModalHeader className="d-flex justify-content-center">
                    <Row className="d-flex justify-content-between ">
                        <Col-1></Col-1>
                        <Col md="10" className="text-center py-5">
                            <img className="" height="150" src={logo} />
                           <p><font size="3">Francisco Alarcon</font></p>
                           <p><font size="4">320 653 2092</font></p>
                        </Col>
                        <Col-1></Col-1>
                    </Row>
                    </ModalHeader>
                    </div>
                </View>   
                <ModalBody>
                <div className="container">
                  <img className="" height="50" src={face} />
                    <p>Hello. How are you today?</p>
                    <span className="time-right">11:00</span>
                    </div>

                    <div className="container darker">
                    <img className="" height="50" src={face} />
                    <p>Nah, I dunno. Play soccer.. or learn more coding perhaps?</p>
                    <span className="time-left">11:05</span>
                </div>
                </ModalBody>
                <ModalFooter>
                </ModalFooter>
                </Modal>
            </div>
            <Imagenes/>
            <Foot/>
        </div>
            
      );
    };
  }

  export default Calificacion;