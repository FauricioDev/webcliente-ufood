import React, { Component } from 'react';
import  {Button, View, Row, Col } from 'mdbreact';
import './calificacion.css';
import tristecolor from '../../img/icons/tristecolor.png'
import tristegris from '../../img/icons/tristegris.png'
import regulargris from '../../img/icons/regulargris.png';
import regularcolor from '../../img/icons/regularcolor.png';
import masogris from '../../img/icons/masogris.png';
import masocolor from '../../img/icons/masocolor.png';
import biengris from '../../img/icons/biengris.png';
import biencolor from '../../img/icons/biencolor.png';
import enamoradogris from '../../img/icons/enamoradogris.png';
import enamoradocolor from '../../img/icons/enamoradocolor.png';
import url from '../../config/url';

import urunner from '../../img/icons/urunner.png'


class Section2 extends Component {
   
    state={
        triste: false,
        regular: false,
        maso: false,
        bien: false,
        enamorado: false,
        points: 0,
        oberservacion: ""
    }

    createCalification =async()=>{
        
        const { points, observacion}= this.state;
        // const { data, token } = this.props;
        const token = await localStorage.getItem('token');
        const idHijo = await localStorage.getItem('idCarritoHijo');

        console.log("========================");
        console.log("token:",token, points, observacion);
        console.log("========================");
        console.log("------------------------");
        console.log("========================");
        console.log("id:",idHijo);
        console.log("========================");

        // id carrito hijo

        const body ={
            point: points,
            message: observacion
        }
        console.log("========================");
        console.log("body:",body);
        console.log("========================");

        if(points !== 0 && observacion !=="" ){

            fetch(`${url}gst-ordenes/points/${idHijo}/`,
            {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
                body: JSON.stringify(body)
            })
            .then(res => res.json())
            .then(response =>{
                console.log('respuesta al crear la calificacion', response);
                alert("Gracias por contarnos tu experiencia, con el fin de mejorar");
                window.location.pathname='/';

            })
            .catch(err => console.error('error al crear la calificacion', err));
        } else {
            alert("porfavor cuentanos que tal fue tu experiencia");
        }
    }

    render() {
      return(
        <div className="container">
                <View className="py-5">
                    <div className="row d-flex justify-content-center">
                        <div className="col-3">
                        </div>
                        <div className="col-6 text-center">
                            <p className="h2-responsive font-weight-bold">Califica tu experiencia</p>
                            <p className="grey-text">Ayudanos a mejorar contandonos <br/>como te fue en tu servicio</p>
                            <div className="d-flex justify-content-center" style={{display:'flex'}}>
                                <img className="px-1" height="70" src={
                                    (this.state.triste) ?
                                    tristecolor
                                    : tristegris
                                }
                                onClick={()=>this.setState({points:1, triste: true, maso: false, enamorado: false })} 
                                />
                                {/* <img className="px-1" height="70" src={
                                    (this.state.regular) ?
                                    regularcolor
                                    : regulargris
                                } 
                                /> */}
                                <img className="px-1" height="70" src={
                                    (this.state.maso) ?
                                    masocolor
                                    : masogris
                                } 
                                onClick={()=>this.setState({points:3, triste: false, maso: true, enamorado: false})}
                                />
                                {/* <img className="px-1" height="70" src={
                                    (this.state.triste) ?
                                    tristecolor
                                    : tristegris
                                } 
                                /> */}
                                <img className="px-1" height="70" src={
                                    (this.state.enamorado) ?
                                    enamoradocolor
                                    : enamoradogris
                                } 
                                onClick={()=>this.setState({points: 5, triste: false, maso: false, enamorado: true})}
                                />
                                {/* <img className="px-1" height="70" src={triste} 
                                                                    onClick={()=>console.log('triste Feliz')}

                                />
                                <img className="px-1" height="70" src={triste} />
                                <img className="px-1" height="70" src={triste} />
                                <img className="px-1" height="70" src={triste} /> */}
                            </div>
                            <p className="h5-responsive font-weight-bold grey-text">¿ Que paso con tu pedido ?</p>
                            <Row className="d-flex justify-content-center pb-4">
                                <Col-6>
                                    <Button 
                                    onClick={()=>this.setState({oberservacion: "opcion1"})}
                                    outline color="danger">Opcion 1</Button>
                                </Col-6>
                                <Col-6>
                                    <Button 
                                    onClick={()=>this.setState({oberservacion: "opcion2"})}
                                    outline color="danger">Opcion 2</Button>
                                </Col-6>
                            </Row>
                            <Row className="d-flex justify-content-center">
                                <Col-6>
                                    <Button
                                        onClick={()=>this.setState({oberservacion: "opcion3"})}
                                     outline color="danger">Opcion 3</Button>
                                </Col-6>
                                <Col-6>
                                    <Button 
                                    onClick={()=>this.setState({oberservacion: "opcion4"})}
                                    outline color="danger">Opcion 4</Button>
                                </Col-6>
                            </Row>
                            <br/>
                            <Button
                            onClick={()=> this.createCalification()}
                            className="badge-pill py-3 px-5" color="danger" size="md">Enviar</Button><br/>        
                            <Button 
                            onClick={()=>this.createCalification()}
                            className="badge-pill" color="danger" size="md">Enviar y <br/>realizar otro pedido</Button>        
                        </div>
                        <div className="col-3">
                        </div>                        
                    </div>
                </View>
        </div>
            
      );
    };
  }

  export default Section2;