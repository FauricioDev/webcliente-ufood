import React, { Component } from 'react';
import  {Row, Col,View } from 'mdbreact';
import './calificacion.css';

import reloj from '../../img/icons/reloj.png'



class Section1 extends Component {
    
  
    render() {
      return(
            <div>
                
                <View className="bggreen1 text-center">
                    <Row>
                        <Col md="5 d-flex justify-content-end">
                            <img src={reloj} height="100" className="img-fuid"/>
                        </Col>
                        <Col md="7 text-left">
                            <p className="h3-responsive grey-text">Estado de tu Orden:</p>
                            <p className="h4-responsive"><strong>Muchas Gracias por utilizar UFood, porfavor tomate 1 minuto y califica nuestro servicio.</strong></p>
                        </Col>
                    </Row>
                    
                </View>

                <div className="container-fluid">
                    <div id="wrapper" className="container-fluid py-4">
                        <span class='baricon'>1</span>
                        <span id="bar1" class='progress_bar'></span>
                        <span class='baricon'>2</span>
                        <span id="bar2" class='progress_bar'></span>
                        <span class='baricon'>3</span>
                        <span id="bar3" class='progress_bar2'></span>
                        <span class='baricon2'>4</span>
                        <span id="bar4" class='progress_bar2'></span>
                        <span class='baricon2'>5</span>
                    </div>
                </div>
            </div>
            
      );
    };
  }

  export default Section1;