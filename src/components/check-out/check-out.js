import React, { Component } from 'react';
import  {Stepper, Step, Container, Row, Col, Button, Footer, Table, Input, Fa } from 'mdbreact';
import './check-out.css'
// import chulo from '../../img/icons/check.png'
// import chulo from '../../img/icons/chulo.png'
import chulo from '../../img/icons/check1.png'
import Foot from '../foot/foot';
import { BrowserRouter as Router, Link } from "react-router-dom";


class Check extends Component {
   
  
    render() {
      return(
            <div className="container text-center py-4">
                     <img className="img-fluid" src={chulo} alt="finalizado" />
                     <p className="grey-text py-5">Gracias por utilizar Ufood, tu orden esta siendo procesada.</p>
                    
                    <Link to="/Pedidos">
                     <Button href="./check-out-tracker" className="badge-pill" color="danger">Seguimiento de tu pedido</Button>
                    </Link>
            </div>
      );
    };
  }
  export default Check;