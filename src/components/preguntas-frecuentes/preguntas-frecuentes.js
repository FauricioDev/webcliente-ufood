import React, { Component } from 'react';
import  { Input, Fa } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import logo from '../../img/logo-blanco.png'
import Foot from '../foot/foot';
import './preguntas-frecuentes';
import { CardGroup, Card, CardBody, CardImage, CardText, CardTitle, View, Button } from 'mdbreact';



class Preguntas extends Component {
   

    render() {
        return(
            
        <div>
            <div className="container py-5" style={{marginTop:150, textAlign:'justify'}}>
            <p className="h1-responsive text-center">Preguntas Frecuentes</p>
            <p className="grey-text text-center mb-5">This Agreement was last modified on 18 April 2016.</p>
            <div className="container py-5 px-5">
                <p className="h4-responsive">Información de Envío</p><br/>
                    <div className="row">
                        <div className="col-md-6 col-xs-12">
                            <p><strong>¿Como funcionan nuestros servicios?</strong></p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                        <div className="col-md-6 col-xs-12">
                            <p><strong>¿Cuanto tarda mi pedido en llegar a mi?</strong></p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                    </div>
                    <div className="row pt-5">
                        <div className="col-md-6 col-xs-12">
                            <p><strong>¿Como le haog segimiento a mi pedido?</strong></p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                        <div className="col-md-6 col-xs-12">
                            <p><strong>¿Necesito una cuenta para hacer pedido?</strong></p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                    </div>
            </div>
              
             
            </div>
                <Foot/>
        </div>

        )
    }
}

export default Preguntas;

