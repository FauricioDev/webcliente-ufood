import React, { Component } from "react";
import {
  Stepper,
  Step,
  Container,
  Row,
  Col,
  Button,
  View,
  Mask
} from "mdbreact";
import "./check-out-paso-1.css";
import CardViewProduct from "./componente/CardViewProduct";
import { connect } from "react-redux";
import url from '../../config/url';

var precioTotal=0;
class Productos extends Component {
  constructor(props) {
    super(props);
    this.state={
      ordenCompleta:null,
      precioCompleto:0
    }
  }
  
  componentDidMount=async()=> {
    let ordenCompleta = localStorage.getItem('ordenes');
        ordenCompleta = JSON.parse(ordenCompleta);
        this.setState({ordenCompleta})
    console.log('Soy la orden completa',ordenCompleta);
  }
  
  updateOrder=()=>{
    const { ordenCompleta } = this.state;
    console.log('el precio total de la orden es: ', precioTotal);
    ordenCompleta['total'] = precioTotal;
    localStorage.setItem('ordenes', JSON.stringify(ordenCompleta));
  }
  
  render() {
    return (
      <View className="container py-5">
        <Row className="">
          <Col md="6" xs="12">
            <p>Producto</p>
          </Col>
          <Col md="2" xs="12">
            <p>Precio</p>
          </Col>
          <Col md="2" xs="12">
            <p>Cantidad</p>
          </Col>
          <Col md="2" xs="12">
            <p>Total</p>
          </Col>
        </Row>
        {
          (this.state.ordenCompleta)?
          
            this.state.ordenCompleta.productos.map(orden=>{
              console.log('el primer map',orden);
             return orden.productos.map((item,index, array)=>{
              precioTotal = parseInt(item.total) + parseInt(precioTotal);
              console.log(precioTotal);
              // this.setState({precioCompleto: precioTotal})
                console.log('el segundo map', item);
                return(
                    <CardViewProduct
                      key={index}
                      nombre={item.nombre_producto}
                      total={item.total}
                      cantidad={item.cantidad}
                    />
                  );
              });
            })
          
          : <h1>No tienes productos en el carrito por el momento</h1>
        }
        <Row className="d-flex align-items-center">
          <Col-3>
            <input
              type="text"
              id="cupon"
              name="cupon"
              className="form-control py-1"
              style={{ borderRadius: "50px" }}
              placeholder="Cupon code"
            />
          </Col-3>
          <Col-3>
            <Button className="badge-pill" color="danger" size="md">
              Aplicar cupon
            </Button>
          </Col-3>
        </Row>
        <Row className="d-flex justify-content-end pt-4">
          <Col-2>
            <p>Orden con URunner</p>
            <p>
              Total: <strong>$ {precioTotal}</strong>
            </p>
            <Button
              className="badge-pill"
              color="danger"
              size="md"
              onClick={()=>{
                if(this.state.ordenCompleta){
                  this.props.nextStep();
                  this.updateOrder();
                }else{
                  alert('no tienes productos en el carrito ')
                }
                            }
                          }
            >
              Procede al checkout
            </Button>
          </Col-2>
        </Row>
      </View>
    );
  }
}
const mapStateToProps = (state, props) => {
  console.log('los state', state);
  return {
    orden: state.orden
  };
};

//   export default Productos;


export default connect(
  mapStateToProps,
  null
)(Productos);