import React, { Component } from 'react';
import  {Stepper, Container, Row, Col, Button, View, Mask } from 'mdbreact';
import './check-out-paso-1.css';

import Steps, { Step } from 'rc-steps';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';

import reloj from '../../img/icons/reloj.png'
import Foot from '../foot/foot';
import Imagenes from '../imagenes/imagenes';
import urunner from '../../img/icons/urunner.png'
import logo from '../../img/logo-blanco.png'
import image from '../../img/02.jpg'

import './check-out-paso-1.css'


export const Navega =(props)=> {
     {
      return(
        <Steps 
          labelPlacement="vertical"
          current={props.estado}
          >
            <Step 
              title="Tu pedido"
               />
            <Step title="Checkout" />
            <Step title="Pedido Tomado" />
        </Steps>
      );
    };
  }

  export default Navega;