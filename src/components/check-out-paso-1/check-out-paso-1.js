import React, { Component } from 'react';
import  {Stepper, Step, Container, Row, Col, Button, View, Mask } from 'mdbreact';
import './check-out-paso-1.css';
// import chulo from '../../img/icons/check.png'
// import chulo from '../../img/icons/chulo.png'
import reloj from '../../img/icons/reloj.png'
import Foot from '../foot/foot';
import Imagenes from '../imagenes/imagenes';
import urunner from '../../img/icons/urunner.png'
import logo from '../../img/logo-blanco.png'
import image from '../../img/02.jpg'
import Navega from './navega';
import Productos from './productos';
import Section3 from '../orden-pedido/section3';
import Paso2 from '../check-out-paso-2/check-out-paso-2';
import CheckOut from '../check-out/check-out';
import Menu from '../menu/menu';

class Paso1 extends Component {
  constructor(props){
    super(props);
    this.state={
      paso: 0,
    }
  }
  nextStep =()=>this.setState({paso: this.state.paso + 1});
  
    render() {
      const {paso}= this.state;
      return(

        <div>
                  <div className="App-header" style={{marginBottom: 200}}>
                    <Menu />
                  </div>
                <Navega estado={this.state.paso}/>
                <br/><br/>
                {
                  (paso == 0)&&
                  <Productos 
                    nextStep ={()=>this.nextStep()}
                  />
                  }
                {(paso ==1)&&<Paso2 
                    nextStep ={()=>this.nextStep()}

                />}  
                {(paso==2)&& <CheckOut />}
                
                {/* <Section3/> */}
                <Foot/>
            </div>
            
      );
    };
  }

  export default Paso1;