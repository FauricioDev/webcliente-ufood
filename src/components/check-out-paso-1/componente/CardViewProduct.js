import React, { Component } from 'react';
import  {Stepper, Step, Container, Row, Col, Button, View, Mask } from 'mdbreact';
import image from '../../../img/02.jpg';

export const CardViewProduct  =(props)=>{
return (
    <Row className="py-4">
        <Col md="2">
            <img src={image} height="100" />
        </Col>
        <Col md="4">
            <p><h4>{props.nombre}</h4></p>
            {/* <p>Toppings</p>
            <p>Crust</p>
            <p>Size</p> */}
        </Col>
        <Col md="2">
            <p>${props.total}</p>
        </Col>
        <Col md="2">
            <input type="number" id="cantidad" name="cantidad" className="form-control py-1" style={{borderRadius:'50px'}} value={props.cantidad} />
        </Col>
        <Col md="2">
            <p>${props.total}</p>
        </Col>
    </Row>
        );
};

export default CardViewProduct;
