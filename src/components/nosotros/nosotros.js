import React, { Component } from 'react';
import  { Input, Fa } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import './nosotros.css'
import logo from '../../img/logo-blanco.png'
import Foot from '../foot/foot';
import { CardGroup, Card, CardBody, CardImage, CardText, CardTitle, View, Button } from 'mdbreact';



class Nosotros extends Component {
   

    render() {
        return(
            
        <div>
            <div className="container py-5" style={{marginTop:150}}>
            <p className="h1-responsive text-center">Acerca de Nosotros</p>
            <p className="grey-text text-center mb-5">Somo una empresa familiar de segunda generación, fundada en 1972.</p>
            <View className="pt-3">
                <CardGroup deck className="mb-4">
                    <Card>
                        <CardImage src="https://mdbootstrap.com/img/Photos/Others/images/16.jpg" height="286" alt="Card image cap" top hover overlay="white-slight"/>
                        <CardBody className="">
                            <CardTitle tag="h5"><strong>Family of Creators</strong></CardTitle>
                            <CardText className="grey-text">Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcomper. Proin sed ultrices erat.</CardText>
                        </CardBody>
                    </Card>

                    <Card>
                        <CardImage src={logo} alt="Card image cap" height="286" top hover overlay="white-slight"/>
                        <CardBody className="">
                            <CardTitle tag="h5"><strong>Made the Quality Way</strong></CardTitle>
                            <CardText className="grey-text">Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. <br/> <br/>
                            Be Bold // Do Everything Better <br/>
                            Be Open // Work Stronger Together</CardText>
                        </CardBody>
                    </Card>

                    <Card>
                        <CardImage src={logo} alt="Mc Donald's" height="286" top hover overlay="white-slight"/>
                        <CardBody className="">
                            <CardTitle tag="h5"><strong>Taste Driven</strong></CardTitle>
                            <CardText className="grey-text">Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcomper. Proin sed ultrices erat.</CardText>
                        </CardBody>
                    </Card>
                    
                </CardGroup>
                
              </View>
              
             
            </div>
                <Foot/>
        </div>

        )
    }
}

export default Nosotros;

