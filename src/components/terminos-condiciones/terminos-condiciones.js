import React, { Component } from 'react';
import  { Input, Fa } from 'mdbreact';
import {Tabs, Tab} from 'react-bootstrap-tabs';
import './terminos-condiciones.css'
import logo from '../../img/logo-blanco.png'
import Foot from '../foot/foot';
import { CardGroup, Card, CardBody, CardImage, CardText, CardTitle, View, Button } from 'mdbreact';



class Terminos extends Component {
   

    render() {
        return(
            
        <div>
            <div className="container py-5" style={{marginTop:150}}>
            <p className="h1-responsive text-center">Terminos y condiciones</p>
            <p className="grey-text text-center mb-5">This Agreement was last modified on 18 April 2016.</p>
            <View className="container" style={{textAlign:'justify'}}>
                <hr/>
                <p className="h4-responsive">Intellectual Property</p><br/>
                <ol>
                    <li value="1">Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</li>
                    <li value="2">Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</li>
                    <li value="3">Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</li>
                    <li value="4">Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</li>
                    <li value="5">Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</li>
                    <li value="6">Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</li>
                </ol>
                <p className="h4-responsive pt-5">Termination</p><br/>
                <ol>
                    <li value="1">Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</li>
                    <li value="2">Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</li>
                    <li value="3">Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</li>
                    <li value="4">Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</li>
                </ol>
                <p className="h4-responsive pt-5">Changes To This Agreement</p><br/>
                    <p>Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur</p>
                <p className="h4-responsive pt-5">Contact Us</p><br/>
                <p>Lorem ipsum dolor, consecutur Lorem ipsum dolor, consecuturLorem ipsum dolor, consecuturLorem ipsum dolor, consecutur <a href="./contactenos" style={{color:'black'}}><strong>Contact form.</strong></a></p>
            </View>
              
             
            </div>
                <Foot/>
        </div>

        )
    }
}

export default Terminos;

